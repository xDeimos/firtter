package controller;

public class Event {

    public int EventId;
    public String FInicio;
    public String FTermino;
    public int EventType;
    public String EventRequest;

    public Event(int EventId, String FInicio, String FTermino,
                 int EventType, String EventRequest) {
        this.EventId = EventId;
        this.FInicio = FInicio;
        this.FTermino = FTermino;
        this.EventType = EventType;
        this.EventRequest = EventRequest;
    }

    public String toString() {
        return this.EventId + " " + this.FInicio + " " + this.FTermino + " " +
                this.EventType + " " + this.EventRequest;
    }
}
