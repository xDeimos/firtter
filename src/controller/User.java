package controller;

public class User {

    public int userid;
    public String nickname;
    public String email;
    public String password;
    public String sex;

    public User(String Nickname, String Email, String Password, String Sex, int userid) {
        this.nickname = Nickname;
        this.email = Email;
        this.password = Password;
        this.sex = Sex;
        this.userid = userid;
    }

    @Override
    public String toString() {
        return this.userid + " " + this.email + " " + this.password + " " + this.sex;
    }

}
