package controller;

import android.graphics.Bitmap;

public class ImageAdapter {
    public Bitmap bitmap;
    public int photoid;
    public int eventid;
    public String email;
    public int fragment;
    public int width;
    public int height;

    public ImageAdapter(Bitmap bitmap, int photoid, int eventid, String email, int fragment, int width, int height) {
        super();
        this.bitmap = bitmap;
        this.photoid = photoid;
        this.eventid = eventid;
        this.email = email;
        this.fragment = fragment;
        this.width = width;
        this.height = height;
    }
}
