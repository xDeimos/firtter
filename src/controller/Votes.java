package controller;

/**
 * Created by deimos on 18-11-14.
 */
public class Votes {
    public int votesid;
    public String email;
    public int photoid;
    public int eventid;

    public Votes(int votesid, String email, int photoid, int eventid) {
        this.votesid = votesid;
        this.email = email;
        this.photoid = photoid;
        this.eventid = eventid;
    }

    public String toString() {
        return this.votesid + " " + this.email + " " + this.photoid + " " + this.eventid;
    }
}
