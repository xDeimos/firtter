package controller;

public class ServerTime {

    public String simpleDateFormat;

    public ServerTime(String simpleDateFormat) {
        this.simpleDateFormat = simpleDateFormat;
    }

    public String toString() {
        return this.simpleDateFormat;
    }
}
