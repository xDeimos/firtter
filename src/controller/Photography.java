package controller;

public class Photography {

    public int photoid;
    public byte[] photo;
    public String email;
    public int event;
    public int voto;

    public Photography(byte[] photo, String email, int event, int photoid, int voto) {
        this.photo = photo;
        this.email = email;
        this.event = event;
        this.photoid = photoid;
        this.voto = voto;
    }

    public String toString() {
        return this.photoid + " " + this.email + " " + this.event + " " + this.photo + " " + this.voto;
    }
}