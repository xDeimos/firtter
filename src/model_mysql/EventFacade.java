package model_mysql;

import android.util.Log;
import controller.Event;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class EventFacade {

    public Event factory(ResultSet rs) throws SQLException {

        int EventId = rs.getInt("EventId");
        String FInicio = rs.getString("FInicio");
        String FTermino = rs.getString("FTermino");
        int EvenetType = rs.getInt("EventType");
        String EventRequest = rs.getString("EventRequest");

        return new Event(EventId, FInicio, FTermino, EvenetType, EventRequest);
    }

    public Event findEvent() {
        Connect connect = null;
        PreparedStatement ps = null;
        Event event = null;

        String sql = "SELECT * FROM `EVENT` WHERE `EventId`=(SELECT max(`EventId`) FROM `EVENT`)";

        try {
            connect = new Connect();
            ps = connect.getConexion().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            if (rs.next())
                event = factory(rs);

        } catch (SQLException ex) {
            Log.e("Error -> DeviceFacade -> findById", ex.getMessage());

        } finally {
            if (connect != null)
                connect.close();
        }
        return event;
    }
}
