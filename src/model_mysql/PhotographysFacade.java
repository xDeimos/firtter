package model_mysql;

import android.util.Log;
import controller.Photography;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PhotographysFacade {

    public Photography factory(ResultSet rs) throws SQLException {

        int photoid = rs.getInt("photoid");
        byte[] photo = rs.getBytes("photo");
        String email = rs.getString("email");
        int event = rs.getInt("event");
        int voto = rs.getInt("voto");

        return new Photography(photo, email, event, photoid, voto);
    }

    public Photography findPhoto(String email, int event) {

        Connect connect = null;
        PreparedStatement ps = null;
        Photography photo = null;

        String sql = "SELECT * FROM PHOTOGRAPHY WHERE email = ? AND event = ?";

        try {
            connect = new Connect();
            ps = connect.getConexion().prepareStatement(sql);
            ps.setString(1, email);
            ps.setInt(2, event);
            ResultSet rs = ps.executeQuery();
            if (rs.next())
                photo = factory(rs);

        } catch (SQLException ex) {
            Log.e("Error -> DeviceFacade -> findById", ex.getMessage());

        } finally {
            if (connect != null)
                connect.close();
        }
        return photo;
    }

    public List<Photography> photographies(String email) {

        List<Photography> photoList = null;
        Connect connect = null;
        PreparedStatement ps = null;
        String sql = "SELECT * FROM PHOTOGRAPHY WHERE email=?";
        photoList = new ArrayList<Photography>();
        try {

            connect = new Connect();
            ps = connect.getConexion().prepareStatement(sql);
            ps.setString(1, email);
            ResultSet rs = ps.executeQuery();
            while (rs.next())
                photoList.add(factory(rs));

        } catch (SQLException ex) {
            Log.e("Error -> DeviceFacade -> findById", ex.getMessage());
        } finally {
            if (connect != null)
                connect.close();
        }
        return photoList;
    }

    public List<Photography> photographiesEvent(int event) {

        List<Photography> photoList = null;
        Connect connect = null;
        PreparedStatement ps = null;
        String sql = "SELECT * FROM PHOTOGRAPHY WHERE event=?";
        photoList = new ArrayList<Photography>();
        try {

            connect = new Connect();
            ps = connect.getConexion().prepareStatement(sql);
            ps.setInt(1, event);
            ResultSet rs = ps.executeQuery();
            while (rs.next())
                photoList.add(factory(rs));

        } catch (SQLException ex) {
            Log.e("Error -> DeviceFacade -> findById", ex.getMessage());
        } finally {
            if (connect != null)
                connect.close();
        }
        return photoList;
    }

    public Photography save(Photography photo) {

        Connect connect = null;
        PreparedStatement ps = null;
        String sql = "INSERT INTO PHOTOGRAPHY (photo,email,event,voto) VALUES (?,?,?,?)";

        try {

            connect = new Connect();
            ps = connect.getConexion().prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            ps.setBytes(1, photo.photo);
            ps.setString(2, photo.email);
            ps.setInt(3, photo.event);
            ps.setInt(4, photo.voto);
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next())
                photo.photoid = rs.getInt(1);

        } catch (SQLException ex) {
            Log.e("Error -> DeviceFacade -> findById", ex.getMessage());
        } finally {
            if (connect != null)
                connect.close();
        }
        return photo;
    }

    public Photography voto(String email, int eventid) {

        Connect connect = null;
        PreparedStatement ps = null;
        String sql = "UPDATE PHOTOGRAPHY SET `voto` = `voto`+1 WHERE email = ? AND eventid = ?";
        Photography photo = null;

        try {
            connect = new Connect();
            ps = connect.getConexion().prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            ps.setString(1, email);
            ps.setInt(2, eventid);
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next())
                photo.photoid = rs.getInt(1);

        } catch (SQLException ex) {
            Log.e("Error -> DeviceFacade -> findById", ex.getMessage());
        } finally {
            if (connect != null)
                connect.close();
        }
        return photo;
    }

}