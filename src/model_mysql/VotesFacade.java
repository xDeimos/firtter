package model_mysql;

import android.util.Log;
import controller.Votes;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class VotesFacade {
    public Votes factory(ResultSet rs) throws SQLException {

        int votesid = rs.getInt("votesid");
        String email = rs.getString("email");
        int photoid = rs.getInt("photoid");
        int eventid = rs.getInt("eventid");

        return new Votes(votesid, email, photoid, eventid);
    }

    public List<Votes> voteses(String email) {

        List<Votes> votesList = null;
        Connect connect = null;
        PreparedStatement ps = null;
        String sql = "SELECT * FROM VOTED WHERE email=?";
        votesList = new ArrayList<Votes>();
        try {

            connect = new Connect();
            ps = connect.getConexion().prepareStatement(sql);
            ps.setString(1, email);
            ResultSet rs = ps.executeQuery();
            while (rs.next())
                votesList.add(factory(rs));

        } catch (SQLException ex) {
            Log.e("Error -> DeviceFacade -> findById", ex.getMessage());
        } finally {
            if (connect != null)
                connect.close();
        }
        return votesList;
    }

    public Votes save(Votes votes) {

        Connect connect = null;
        PreparedStatement ps = null;
        String sql = "INSERT INTO VOTED (email,photoid,eventid) VALUES (?,?,?)";

        try {

            connect = new Connect();
            ps = connect.getConexion().prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            ps.setString(1, votes.email);
            ps.setInt(2, votes.photoid);
            ps.setInt(3, votes.eventid);
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next())
                votes.votesid = rs.getInt(1);

        } catch (SQLException ex) {
            Log.e("Error -> DeviceFacade -> findById", ex.getMessage());
        } finally {
            if (connect != null)
                connect.close();
        }
        return votes;
    }


}
