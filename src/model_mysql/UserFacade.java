package model_mysql;

import android.util.Log;
import controller.User;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserFacade {

    public User factory(ResultSet rs) throws SQLException {

        int userid = rs.getInt("userid");
        String nickname = rs.getString("nickname");
        String email = rs.getString("email");
        String password = rs.getString("password");
        String sex = rs.getString("sex");

        return new User(nickname, email, password, sex, userid);
    }

    public User find(String mail) {

        Connect connect = null;
        PreparedStatement ps;
        User user = null;

        String sql = "SELECT * FROM USER WHERE email='" + mail + "'";

        try {
            connect = new Connect();
            ps = connect.getConexion().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            if (rs.next())
                user = factory(rs);

        } catch (SQLException ex) {
            Log.e("Error -> DeviceFacade -> findById", ex.getMessage());

        } finally {
            if (connect != null)
                connect.close();
        }
        return user;
    }

    public User save(User user) {
        Connect connect = null;
        PreparedStatement ps;
        String sql = "INSERT INTO USER (nickname,email,password,sex) VALUES (?,?,?,?)";

        try {

            connect = new Connect();
            ps = connect.getConexion().prepareStatement(sql,
                    PreparedStatement.RETURN_GENERATED_KEYS);
            ps.setString(1, user.nickname);
            ps.setString(2, user.email);
            ps.setString(3, user.password);
            ps.setString(4, user.sex);
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next())
                user.userid = rs.getInt(1);

        } catch (SQLException ex) {
            Log.e("Error -> DeviceFacade -> findById", ex.getMessage());
        } finally {
            if (connect != null)
                connect.close();
        }
        return user;
    }

    public User login(String email, String password) {

        Connect connect = null;
        PreparedStatement ps;
        User user = null;
        String sql = "SELECT * FROM USER  WHERE email=? AND password=?";

        try {
            connect = new Connect();
            ps = connect.getConexion().prepareStatement(sql);
            ps.setString(1, email);
            ps.setString(2, password);
            ResultSet rs = ps.executeQuery();
            if (rs.next())
                user = factory(rs);

        } catch (SQLException ex) {
            Log.e("Error -> DeviceFacade -> findById", ex.getMessage());

        } finally {
            if (connect != null)
                connect.close();
        }
        return user;
    }
}