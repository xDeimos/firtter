package model_mysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Connect {

    private Connection conexion = null;
    private String servidor = "frizlincoqueo.cl";
    private String database = "christopher";
    private String usuario = "christopher";
    private String password = "christopher1234";
    private String url = "";

    public Connect() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            url = "jdbc:mysql://" + servidor + "/" + database;
            conexion = DriverManager.getConnection(url, usuario, password);
        } catch (SQLException ex) {
            System.out.println("sql -----" + ex);
        } catch (ClassNotFoundException ex) {
            System.out.println("class---" + ex);
        }
    }

    public Connection getConexion() {
        return conexion;
    }

    public void close() {
        try {
            conexion.close();
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        conexion = null;
    }
}
