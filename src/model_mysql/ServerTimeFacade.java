package model_mysql;

import android.util.Log;
import controller.ServerTime;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;


/**
 * Created by deimos on 13-11-14.
 */
public class ServerTimeFacade {

    public ServerTime factory(ResultSet rs) throws SQLException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        return new ServerTime(sdf.format(rs.getTimestamp("AHORA")));

    }

    public ServerTime find() {

        Connect connect = null;
        PreparedStatement ps;
        ServerTime serverTime = null;

        String sql = "SELECT NOW() as AHORA FROM `foo`";


        try {
            connect = new Connect();
            connect.getConexion().createStatement().execute("SET TIME_ZONE = '-03:00'");
            ps = connect.getConexion().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            if (rs.next())
                serverTime = factory(rs);

        } catch (Exception ex) {
            Log.e("Error -> DeviceFacade -> findById", ex.getMessage());

        } finally {
            if (connect != null)
                connect.close();
        }

        return serverTime;
    }
}
