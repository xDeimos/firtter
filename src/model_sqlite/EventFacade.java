package model_sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import controller.Event;

public class EventFacade {

    public Context context;

    public EventFacade(Context context) {
        this.context = context;
    }

    public void save(Event event) {
        DataBase db = new DataBase(context);
        SQLiteDatabase conn = db.openDataBase();
        ContentValues values = new ContentValues();
        values.put("EVENTID", event.EventId);
        values.put("FINICIO", event.FInicio);
        values.put("FTERMINO", event.FTermino);
        values.put("EVENTTYPE", event.EventType);
        values.put("EVENTREQUEST", event.EventRequest);
        conn.insert("EVENT", null, values);
        conn.close();
    }

    public Event findEvent() {
        Event result = null;

        DataBase db = new DataBase(this.context);
        SQLiteDatabase conn = db.openDataBase();
        String query = "SELECT * FROM EVENT WHERE EventId = (SELECT MAX(EventId) FROM EVENT)";
        Cursor cursor = conn.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            int EventId = cursor.getInt(0);
            String FInicio = cursor.getString(1);
            String FTermino = cursor.getString(2);
            int EventType = cursor.getInt(3);
            String EventRequest = cursor.getString(4);
            result = new Event(EventId, FInicio, FTermino, EventType, EventRequest);
        }
        conn.close();
        return result;
    }

    public Event close() {
        Event result = null;

        DataBase db = new DataBase(this.context);
        SQLiteDatabase conn = db.openDataBase();
        conn.execSQL("DELETE FROM EVENT");
        conn.execSQL("VACUUM");
        conn.close();
        return result;
    }
}
