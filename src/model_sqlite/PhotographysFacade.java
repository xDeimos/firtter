package model_sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import controller.Photography;

import java.util.ArrayList;
import java.util.List;

public class PhotographysFacade {

    public Context context;

    public PhotographysFacade(Context context) {
        this.context = context;
    }

    public void save(Photography photographys) {
        DataBase db = new DataBase(context);
        SQLiteDatabase conn = db.openDataBase();
        ContentValues values = new ContentValues();
        values.put("PHOTO", photographys.photo);
        values.put("EMAIL", photographys.email);
        values.put("EVENT", photographys.event);
        values.put("PHOTOID", photographys.photoid);
        values.put("VOTO", photographys.voto);
        conn.insert("PHOTOGRAPHY", null, values);
        conn.close();
    }

    public Photography findPhotEvent(int Event) {
        Photography result = null;

        DataBase db = new DataBase(this.context);
        SQLiteDatabase conn = db.openDataBase();
        String query = "SELECT rowid,* FROM PHOTOGRAPHY WHERE EVENT='" + Event
                + "'";
        Cursor cursor = conn.rawQuery(query, null);

        if (cursor.moveToFirst()) {

            int photoid = cursor.getInt(1);
            byte[] photo = cursor.getBlob(2);
            String email = cursor.getString(3);
            int event = cursor.getInt(4);
            int voto = cursor.getInt(5);
            result = new Photography(photo, email, event, photoid, voto);
        }
        conn.close();
        return result;
    }

    public List<Photography> photographyList() {
        List<Photography> result = null;

        DataBase db = new DataBase(this.context);
        SQLiteDatabase conn = db.openDataBase();
        String query = "SELECT ROWID,* from PHOTOGRAPHY";
        Cursor cursor = conn.rawQuery(query, null);
        result = new ArrayList<Photography>();
        if (cursor.moveToFirst()) {
            do {
                int photoid = cursor.getInt(1);
                byte[] photo = cursor.getBlob(2);
                String email = cursor.getString(3);
                int event = cursor.getInt(4);
                int voto = cursor.getInt(5);
                result.add(new Photography(photo, email, event, photoid, voto));
            } while (cursor.moveToNext());
        }
        conn.close();
        return result;

    }

    public Photography close() {
        Photography result = null;

        DataBase db = new DataBase(this.context);
        SQLiteDatabase conn = db.openDataBase();
        conn.execSQL("DELETE FROM PHOTOGRAPHY");
        conn.execSQL("VACUUM");
        conn.close();
        return result;
    }


}