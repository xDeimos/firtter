package model_sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import controller.User;

public class UserFacade {

    public Context context;

    public UserFacade(Context context) {
        this.context = context;
    }

    public void save(User user) {
        DataBase db = new DataBase(context);
        SQLiteDatabase conn = db.openDataBase();
        ContentValues values = new ContentValues();
        values.put("NICKNAME", user.nickname);
        values.put("EMAIL", user.email);
        values.put("PASSWORD", user.password);
        values.put("SEX", user.sex);
        values.put("USERID", user.userid);
        conn.insert("USER", null, values);
        conn.close();

    }

    public User find() {
        User result = null;

        DataBase db = new DataBase(this.context);
        SQLiteDatabase conn = db.openDataBase();
        String query = "SELECT rowid,* from USER";
        Cursor cursor = conn.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            int userid = cursor.getInt(1);
            String nickname = cursor.getString(2);
            String email = cursor.getString(3);
            String password = cursor.getString(4);
            String sex = cursor.getString(5);
            result = new User(nickname, email, password, sex, userid);
        }

        return result;
    }

    public User close() {
        User result = null;

        DataBase db = new DataBase(this.context);
        SQLiteDatabase conn = db.openDataBase();
        conn.execSQL("DELETE FROM USER");
        conn.execSQL("VACUUM");
        conn.close();
        return result;
    }

}
