package model_sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import controller.Votes;

/**
 * Created by deimos on 18-11-14.
 */
public class VotesFacade {
    public Context context;

    public VotesFacade(Context context) {
        this.context = context;
    }

    public void save(Votes votes) {
        DataBase db = new DataBase(context);
        SQLiteDatabase conn = db.openDataBase();
        ContentValues values = new ContentValues();
        values.put("VOTESID", votes.votesid);
        values.put("EMAIL", votes.email);
        values.put("PHOTOID", votes.photoid);
        values.put("EVENTID", votes.eventid);
        conn.insert("VOTES", null, values);
        conn.close();
    }

    public Votes findall() {
        Votes result = null;

        DataBase db = new DataBase(this.context);
        SQLiteDatabase conn = db.openDataBase();
        String query = "SELECT rowid,* from VOTES";
        Cursor cursor = conn.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            int votesid = cursor.getInt(1);
            String email = cursor.getString(2);
            int photoid = cursor.getInt(3);
            int eventid = cursor.getInt(4);
            result = new Votes(votesid, email, photoid, eventid);
        }

        return result;
    }

    public Votes find(int Photoid, int Eventid) {
        Votes result = null;

        DataBase db = new DataBase(this.context);
        SQLiteDatabase conn = db.openDataBase();
        String query = "SELECT rowid, * from VOTES WHERE PHOTOID = '" + Photoid
                + "' and EVENTID ='" + Eventid + "'";
        Cursor cursor = conn.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            int votesid = cursor.getInt(1);
            String email = cursor.getString(2);
            int photoid = cursor.getInt(3);
            int eventid = cursor.getInt(4);
            result = new Votes(votesid, email, photoid, eventid);
        }
        return result;
    }

    public Votes close() {
        Votes result = null;

        DataBase db = new DataBase(this.context);
        SQLiteDatabase conn = db.openDataBase();
        conn.execSQL("DELETE FROM VOTES");
        conn.execSQL("VACUUM");
        conn.close();
        return result;
    }
}
