package Elements;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import cl.CEDEST.Firtter.R;
import components.ImageView;
import controller.ImageAdapter;
import components.RelativeLayout;
import controller.Votes;
import model_sqlite.UserFacade;

public class Adapter extends ArrayAdapter<ImageAdapter> {

    ImageView imageView;
    ImageView voto;
    LoadingDialogs ld;
    Votes votes;
    int photoid;
    int eventid;
    String email;
    private Context context;

    public Adapter(Context context) {
        super(context, android.R.layout.simple_gallery_item);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        RelativeLayout.LayoutParams layoutbot = new RelativeLayout.LayoutParams(-1, -2);
        layoutbot.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);

        ld = new LoadingDialogs(context);

        photoid = this.getItem(position).photoid;
        eventid = this.getItem(position).eventid;
        email = this.getItem(position).email;


        votes = new model_sqlite.VotesFacade(context).find(photoid, eventid);
        System.out.println(votes + "<---- votes adapter");

        LinearLayout.LayoutParams iconParams = new LinearLayout.LayoutParams(components.LinearLayout.LayoutParams.WRAP_CONTENT, components.LinearLayout.LayoutParams.FILL_PARENT);
        iconParams.height = parent.getWidth() / 10;
        iconParams.width = parent.getWidth() / 10;

        RelativeLayout relativeLayout = new RelativeLayout(this.context);
        LinearLayout layoutBot = new LinearLayout(context);

        layoutBot.setLayoutParams(layoutbot);
        layoutBot.setGravity(Gravity.RIGHT);
        LinearLayout photoLayout = new LinearLayout(context);
        photoLayout.setGravity(Gravity.CENTER);

        if (votes != null) {
            voto = new ImageView(context, R.drawable.icons_0003);
            voto.setLayoutParams(iconParams);
            voto.setEnabled(false);
        } else {
            voto = new ImageView(context, R.drawable.icons_0001);
            voto.setLayoutParams(iconParams);
        }
        ImageView like = new ImageView(context, R.drawable.icons_0000);
        like.setLayoutParams(iconParams);
        Bitmap bitmap = this.getItem(position).bitmap;
        imageView = new ImageView(context);
        imageView.setImageBitmap(bitmap);

        layoutBot.addView(like);
        layoutBot.addView(voto);
        photoLayout.addView(imageView);
        relativeLayout.addView(photoLayout, -1, -2);
        relativeLayout.addView(layoutBot);

        if (this.getItem(position).fragment == 1) {
            imageView.setLayoutParams(new LinearLayout.LayoutParams(this.getItem(position).width, this.getItem(position).height / 2));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setPadding(5, 5, 5, 5);
        }
        if (this.getItem(position).fragment == 2) {
            imageView.setLayoutParams(new LinearLayout.LayoutParams(this.getItem(position).width / 2, this.getItem(position).height / 4));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setPadding(5, 5, 5, 5);
        }
        if (this.getItem(position).fragment == 3) {
            imageView.setLayoutParams(new LinearLayout.LayoutParams(this.getItem(position).width / 3, this.getItem(position).height / 6));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setPadding(5, 5, 5, 5);
        }

        voto.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                AsynTaskVote();
            }
        });

        like.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("selecione like");

            }
        });

        imageView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Estoy en la foto");
            }
        });
        return relativeLayout;
    }

    protected void AsynTaskVote() {

        new AsyncTask<Void, Void, Void>() {
            Votes newVote = null;
            Votes tryvotes = null;

            @Override
            protected void onPreExecute() {
                ld.show();
                super.onPreExecute();
            }

            @Override
            protected Void doInBackground(Void... params) {
                String localEmail = new UserFacade(context).find().email;
                if (votes == null) {
                    new model_mysql.PhotographysFacade().voto(email, eventid);
                    newVote = new Votes(1, localEmail, photoid, eventid);
                    new model_sqlite.VotesFacade(context).save(newVote);
                    new model_mysql.VotesFacade().save(newVote);
                    tryvotes = new model_sqlite.VotesFacade(context).find(photoid, eventid);
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                if (newVote != null) {
                    voto.setImageDrawable(context.getResources().getDrawable(R.drawable.icons_0003));
                    voto.setEnabled(false);
                }
                System.out.println(tryvotes + "tryvotes");
                System.out.println(newVote + "newvote");
                ld.cancel();
                super.onPostExecute(result);
            }
        }.execute();

    }

}
