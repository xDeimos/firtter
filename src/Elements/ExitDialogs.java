package Elements;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import cl.CEDEST.Firtter.Login;
import cl.CEDEST.Firtter.R;
import components.Button;
import components.LinearLayout;
import components.TextView;
import model_sqlite.PhotographysFacade;

public class ExitDialogs extends Dialog implements View.OnClickListener {

    String text = "¿Realmente deseas desconectarte de la aplicacion?\n " +
            "la proxima vez que ingreses en este equipo, " +
            "se demorara un poco mas en cargar tu informacion";

    Display display;
    int displayWidth;
    int displayHeight;

    int white = Color.WHITE;
    int centralGravitiy = Gravity.CENTER;
    int verticalOrientation = LinearLayout.VERTICAL;
    int horizontalOrientacion = LinearLayout.HORIZONTAL;

    LinearLayout.LayoutParams buttonsParams;

    LinearLayout main;
    LinearLayout verticalLayout;
    LinearLayout horizontalLayout;

    TextView textView;

    Button accept;
    Button cancel;

    LoadingDialogs ld;

    Context context;

    public ExitDialogs(Activity activity) {
        super(activity);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#30000000")));

        ld = new LoadingDialogs(activity);

        context = activity;

        display = getWindow().getWindowManager().getDefaultDisplay();
        displayWidth = display.getWidth();
        displayHeight = display.getHeight();

        buttonsParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.FILL_PARENT);
        buttonsParams.setMargins(displayWidth / 30, displayHeight / 40, displayWidth / 30, displayWidth / 40);

        main = new LinearLayout(activity, "#30000000", centralGravitiy, verticalOrientation);
        verticalLayout = new LinearLayout(activity, "#00000000", centralGravitiy, verticalOrientation);
        horizontalLayout = new LinearLayout(activity, "#00000000", centralGravitiy, horizontalOrientacion);

        textView = new TextView(activity, text, white, centralGravitiy);

        accept = new Button(activity, "Salir", "#00000000", white);
        accept.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.cornershape));
        accept.setLayoutParams(buttonsParams);
        accept.setOnClickListener(this);
        accept.setWidth(displayWidth / 3);
        cancel = new Button(activity, "Cancelar", "#00000000", white);
        cancel.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.cornershape));
        cancel.setLayoutParams(buttonsParams);
        cancel.setOnClickListener(this);
        cancel.setWidth(displayWidth / 3);

        horizontalLayout.addView(accept);
        horizontalLayout.addView(cancel);

        verticalLayout.addView(textView);
        verticalLayout.addView(horizontalLayout);
        main.addView(verticalLayout);
        this.setContentView(main);
    }

    @Override
    public void onClick(View v) {
        if (v.equals(accept)) {
            new AsyncTask<Void, Void, Void>() {

                @Override
                protected void onPreExecute() {
                    ld.show();
                    super.onPreExecute();
                }

                @Override
                protected Void doInBackground(Void... params) {
                    new model_sqlite.UserFacade(context).close();
                    new PhotographysFacade(context).close();
                    new model_sqlite.EventFacade(context).close();
                    new model_sqlite.VotesFacade(context).close();
                    return null;
                }

                @Override
                protected void onPostExecute(Void result) {
                    ld.cancel();
                    context.startActivity(new Intent(context, Login.class));
                    super.onPostExecute(result);
                }
            }.execute();
        }
        if (v.equals(cancel)) {
            dismiss();
        }
    }
}
