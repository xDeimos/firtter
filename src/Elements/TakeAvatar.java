package Elements;

import android.content.Context;
import android.view.View;
import cl.CEDEST.Firtter.Contest;
import cl.CEDEST.Firtter.R;
import components.ImageView;
import components.LinearLayout;

/**
 * Created by deimos on 14-11-14.
 */
public class TakeAvatar extends Contest {

    public TakeAvatar(Context context, String sex, ImageView avatarImageView, LinearLayout topLayout,
                      LinearLayout.LayoutParams avatarViewParam, int widthDisplay) {
        if (sex.equals("0")) {
            avatarImageView.setImageDrawable(avatarImageView.getResources().getDrawable(R.drawable.icons2_0000));
            topLayout.addView(avatarImageView);
            avatarImageView.setLayoutParams(avatarViewParam);
            avatarImageView.getLayoutParams().height = widthDisplay / 10;
            avatarImageView.getLayoutParams().width = widthDisplay / 10;
            avatarImageView.setOnClickListener((View.OnClickListener) context);
        }
        if (sex.equals("1")) {
            avatarImageView.setImageDrawable(avatarImageView.getResources().getDrawable(R.drawable.icons2_0001));
            avatarImageView.setLayoutParams(avatarViewParam);
            topLayout.addView(avatarImageView);
            avatarImageView.getLayoutParams().height = widthDisplay / 10;
            avatarImageView.getLayoutParams().width = widthDisplay / 10;
            avatarImageView.setOnClickListener((View.OnClickListener) context);
        }
        if (sex.equals("2")) {
            avatarImageView.setImageDrawable(avatarImageView.getResources().getDrawable(R.drawable.icons2_0002));
            avatarImageView.setLayoutParams(avatarViewParam);
            topLayout.addView(avatarImageView);
            avatarImageView.getLayoutParams().height = widthDisplay / 10;
            avatarImageView.getLayoutParams().width = widthDisplay / 10;
            avatarImageView.setOnClickListener((View.OnClickListener) context);
        }
        if (sex.equals("3")) {
            avatarImageView.setImageDrawable(avatarImageView.getResources().getDrawable(R.drawable.icons2_0003));
            avatarImageView.setLayoutParams(avatarViewParam);
            topLayout.addView(avatarImageView);
            avatarImageView.getLayoutParams().height = widthDisplay / 10;
            avatarImageView.getLayoutParams().width = widthDisplay / 10;
            avatarImageView.setOnClickListener((View.OnClickListener) context);
        }
        if (sex.equals("4")) {
            avatarImageView.setImageDrawable(avatarImageView.getResources().getDrawable(R.drawable.icons2_0004));
            avatarImageView.setLayoutParams(avatarViewParam);
            topLayout.addView(avatarImageView);
            avatarImageView.getLayoutParams().height = widthDisplay / 10;
            avatarImageView.getLayoutParams().width = widthDisplay / 10;
            avatarImageView.setOnClickListener((View.OnClickListener) context);
        }
        if (sex.equals("5")) {
            avatarImageView.setImageDrawable(avatarImageView.getResources().getDrawable(R.drawable.icons2_0005));
            avatarImageView.setLayoutParams(avatarViewParam);
            topLayout.addView(avatarImageView);
            avatarImageView.getLayoutParams().height = widthDisplay / 10;
            avatarImageView.getLayoutParams().width = widthDisplay / 10;
            avatarImageView.setOnClickListener((View.OnClickListener) context);
        }
    }
}
