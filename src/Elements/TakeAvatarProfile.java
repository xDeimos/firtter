package Elements;

import android.content.Context;
import cl.CEDEST.Firtter.R;
import components.ImageView;
import components.LinearLayout;

/**
 * Created by deimos on 14-11-14.
 */
public class TakeAvatarProfile {

    public TakeAvatarProfile(String sex, ImageView avatarImageView, LinearLayout Layout,
                             LinearLayout.LayoutParams avatarViewParam, int widthDisplay) {
        if (sex.equals("0")) {
            avatarImageView.setImageDrawable(avatarImageView.getResources().getDrawable(R.drawable.avatars_0000));
            Layout.addView(avatarImageView);
            avatarImageView.setLayoutParams(avatarViewParam);
            avatarImageView.getLayoutParams().height = widthDisplay * 60 / 100;
            avatarImageView.getLayoutParams().width = widthDisplay * 60 / 100;
        }
        if (sex.equals("1")) {
            avatarImageView.setImageDrawable(avatarImageView.getResources().getDrawable(R.drawable.avatars_0001));
            avatarImageView.setLayoutParams(avatarViewParam);
            Layout.addView(avatarImageView);
            avatarImageView.getLayoutParams().height = widthDisplay * 60 / 100;
            avatarImageView.getLayoutParams().width = widthDisplay * 60 / 100;
        }
        if (sex.equals("2")) {
            avatarImageView.setImageDrawable(avatarImageView.getResources().getDrawable(R.drawable.avatars_0002));
            avatarImageView.setLayoutParams(avatarViewParam);
            Layout.addView(avatarImageView);
            avatarImageView.getLayoutParams().height = widthDisplay * 60 / 100;
            avatarImageView.getLayoutParams().width = widthDisplay * 60 / 100;
        }
        if (sex.equals("3")) {
            avatarImageView.setImageDrawable(avatarImageView.getResources().getDrawable(R.drawable.avatars_0003));
            avatarImageView.setLayoutParams(avatarViewParam);
            Layout.addView(avatarImageView);
            avatarImageView.getLayoutParams().height = widthDisplay * 60 / 100;
            avatarImageView.getLayoutParams().width = widthDisplay * 60 / 100;
        }
        if (sex.equals("4")) {
            avatarImageView.setImageDrawable(avatarImageView.getResources().getDrawable(R.drawable.avatars_0004));
            avatarImageView.setLayoutParams(avatarViewParam);
            Layout.addView(avatarImageView);
            avatarImageView.getLayoutParams().height = widthDisplay * 60 / 100;
            avatarImageView.getLayoutParams().width = widthDisplay * 60 / 100;
        }
        if (sex.equals("5")) {
            avatarImageView.setImageDrawable(avatarImageView.getResources().getDrawable(R.drawable.avatars_0005));
            avatarImageView.setLayoutParams(avatarViewParam);
            Layout.addView(avatarImageView);
            avatarImageView.getLayoutParams().height = widthDisplay * 60 / 100;
            avatarImageView.getLayoutParams().width = widthDisplay * 60 / 100;
        }
    }
}