package Elements;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Display;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import cl.CEDEST.Firtter.R;
import components.ImageView;

public class LoadingDialogs extends Dialog {

    Display display;

    public LoadingDialogs(Context context) {
        super(context);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        display = getWindow().getWindowManager().getDefaultDisplay();

        ImageView image = new ImageView(context);
        image.setImageDrawable(context.getResources().getDrawable(R.drawable.laodingback));
        image.setImageDrawable(context.getResources().getDrawable(R.drawable.laodingshiny));
        RotateAnimation animation = new RotateAnimation(0, 360, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        animation.setInterpolator(new LinearInterpolator());
        animation.setRepeatCount(Animation.INFINITE);
        animation.setDuration(500);
        image.setAnimation(animation);

        this.setContentView(image);
    }
}
