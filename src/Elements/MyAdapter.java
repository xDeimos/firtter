package Elements;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import components.ImageView;
import controller.MyImageAdapter;
import components.RelativeLayout;

/**
 * Created by deimos on 18-11-14.
 */
public class MyAdapter extends ArrayAdapter<MyImageAdapter> {

    ImageView imageView;
    Context context;


    public MyAdapter(Context context) {
        super(context, android.R.layout.simple_gallery_item);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LinearLayout.LayoutParams iconParams = new LinearLayout.LayoutParams(components.LinearLayout.LayoutParams.WRAP_CONTENT, components.LinearLayout.LayoutParams.FILL_PARENT);
        iconParams.height = parent.getWidth() / 10;
        iconParams.width = parent.getWidth() / 10;

        RelativeLayout relativeLayout = new RelativeLayout(this.context);

        LinearLayout photoLayout = new LinearLayout(context);
        photoLayout.setGravity(Gravity.CENTER);

        Bitmap bitmap = this.getItem(position).bitmap;
        imageView = new ImageView(context);
        imageView.setImageBitmap(bitmap);

        photoLayout.addView(imageView);
        relativeLayout.addView(photoLayout, -1, -2);

        if (this.getItem(position).fragment == 1) {
            imageView.setLayoutParams(new LinearLayout.LayoutParams(this.getItem(position).width, this.getItem(position).height / 2));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setPadding(5, 5, 5, 5);
        }
        if (this.getItem(position).fragment == 2) {
            imageView.setLayoutParams(new LinearLayout.LayoutParams(this.getItem(position).width / 2, this.getItem(position).height / 4));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setPadding(5, 5, 5, 5);
        }
        if (this.getItem(position).fragment == 3) {
            imageView.setLayoutParams(new LinearLayout.LayoutParams(this.getItem(position).width / 3, this.getItem(position).height / 6));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setPadding(5, 5, 5, 5);
        }

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Estoy en la foto");
            }
        });
        return relativeLayout;


    }
}
