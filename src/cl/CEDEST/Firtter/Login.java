package cl.CEDEST.Firtter;

import Elements.LoadingDialogs;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputType;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;
import components.*;
import controller.Event;
import controller.Photography;
import controller.User;
import controller.Votes;
import model_mysql.PhotographysFacade;
import model_sqlite.VotesFacade;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Login extends Activity implements View.OnClickListener {

    String mail, pass;

    int widthDisplay;
    int white = Color.WHITE;
    int verticalOrientation = LinearLayout.VERTICAL;
    int horizontalOrientation = LinearLayout.HORIZONTAL;
    int centralGravity = Gravity.CENTER;
    int rightGravity = Gravity.RIGHT;

    Context context;

    RelativeLayout main;

    RelativeLayout.LayoutParams createParams;
    RelativeLayout.LayoutParams logoParams;
    RelativeLayout.LayoutParams inputsParams0;
    RelativeLayout.LayoutParams inputsParams1;
    RelativeLayout.LayoutParams inputsParams2;
    RelativeLayout.LayoutParams buttonParams;

    LinearLayout.LayoutParams createParamsx;
    LinearLayout.LayoutParams logoParamsx;
    LinearLayout.LayoutParams inputsParamsx;
    LinearLayout.LayoutParams buttonParamsx;

    LinearLayout createLayout;
    LinearLayout logoLayout;
    LinearLayout inputsLayout0;
    LinearLayout inputsLayout1;
    LinearLayout inputsLayout2;
    LinearLayout buttonLayout;

    TextView createText;
    TextView spaceText;

    ImageView logoIcon;

    EditText email;
    EditText password;

    Button login;

    LoadingDialogs ld;

    public boolean isEmailValid(String contenido) {
        String regExpn = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";

        CharSequence inputStr = contenido;

        Pattern pattern = Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);

        if (matcher.matches())
            return true;
        else
            return false;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
        widthDisplay = getWindowManager().getDefaultDisplay().getWidth();
        context = this;
        AsyntaskEvent();

        ld = new LoadingDialogs(this);

        createParams = new RelativeLayout.LayoutParams(-1, -2);
        createParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        logoParams = new RelativeLayout.LayoutParams(-1, -2);
        logoParams.addRule(RelativeLayout.BELOW, 11);
        inputsParams0 = new RelativeLayout.LayoutParams(-1, -2);
        inputsParams0.addRule(RelativeLayout.BELOW, 12);
        inputsParams1 = new RelativeLayout.LayoutParams(-1, -2);
        inputsParams1.addRule(RelativeLayout.BELOW, 13);
        inputsParams2 = new RelativeLayout.LayoutParams(-1, -2);
        inputsParams2.addRule(RelativeLayout.BELOW, 14);
        buttonParams = new RelativeLayout.LayoutParams(-1, -1);
        buttonParams.addRule(RelativeLayout.BELOW, 15);

        createParamsx = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.FILL_PARENT);
        createParamsx.setMargins(0, widthDisplay * 3 / 100, 0, widthDisplay * 5 / 100);
        logoParamsx = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.FILL_PARENT);
        logoParamsx.setMargins(0, widthDisplay * 15 / 100, 0, widthDisplay * 15 / 100);
        inputsParamsx = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.FILL_PARENT);
        inputsParamsx.setMargins(0, widthDisplay * 1 / 100, 0, 0);
        buttonParamsx = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.FILL_PARENT);
        buttonParamsx.setMargins(0, widthDisplay / 10, 0, 0);

        main = new RelativeLayout(this, R.drawable.fondo);
        createLayout = new LinearLayout(this, "#80000000", rightGravity, horizontalOrientation);
        createLayout.setLayoutParams(createParams);
        createLayout.setId(11);
        createLayout.setOnClickListener(this);
        logoLayout = new LinearLayout(this, "#80000000", centralGravity, horizontalOrientation);
        logoLayout.setLayoutParams(logoParams);
        logoLayout.setId(12);
        inputsLayout0 = new LinearLayout(this, "#60000000", centralGravity, verticalOrientation);
        inputsLayout0.setLayoutParams(inputsParams0);
        inputsLayout0.setId(13);
        inputsLayout1 = new LinearLayout(this, "#80000000", centralGravity, verticalOrientation);
        inputsLayout1.setLayoutParams(inputsParams1);
        inputsLayout1.setId(14);
        inputsLayout2 = new LinearLayout(this, "#60000000", centralGravity, verticalOrientation);
        inputsLayout2.setId(15);
        inputsLayout2.setLayoutParams(inputsParams2);
        buttonLayout = new LinearLayout(this, "#80000000", centralGravity, verticalOrientation);
        buttonLayout.setLayoutParams(buttonParams);

        String mystring = new String("Create Account      ");
        SpannableString contenido = new SpannableString(mystring);
        contenido.setSpan(new UnderlineSpan(), 0, mystring.length(), 0);
        createText = new TextView(this, "", white, rightGravity);
        createText.setText(contenido);
        createText.setLayoutParams(createParamsx);
        spaceText = new TextView(this, " ", white, centralGravity);

        logoIcon = new ImageView(this, R.drawable.logo);
        logoIcon.setLayoutParams(logoParamsx);

        email = new EditText(this, "E-Mail", white, centralGravity);
        email.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        email.setWidth(widthDisplay);
        email.setBackgroundDrawable(getResources().getDrawable(R.drawable.inputshape));
        email.setLayoutParams(inputsParamsx);
        password = new EditText(this, "Contraseña", white, centralGravity);
        password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        password.setWidth(widthDisplay);
        password.setBackgroundDrawable(getResources().getDrawable(R.drawable.inputshape));
        password.setLayoutParams(inputsParamsx);

        login = new Button(this, "Login", "#00000000", white);
        login.setOnClickListener(this);
        login.setBackgroundDrawable(getResources().getDrawable(R.drawable.cornershape));
        login.setOnClickListener(this);

        createLayout.addView(createText);
        logoLayout.addView(logoIcon);
        inputsLayout0.addView(email);
        inputsLayout1.addView(spaceText);
        inputsLayout2.addView(password);
        buttonLayout.addView(login);
        main.addView(createLayout);
        main.addView(logoLayout);
        main.addView(inputsLayout0);
        main.addView(inputsLayout1);
        main.addView(inputsLayout2);
        main.addView(buttonLayout);

        login.getLayoutParams().width = widthDisplay / 2;
        logoIcon.getLayoutParams().width = widthDisplay * 70 / 100;
        setContentView(main);
    }

    @Override
    public void onClick(View v) {
        if (v.equals(login)) {
            String contenido = email.getText().toString();
            mail = email.getText().toString();
            pass = password.getText().toString();
            mail = mail.toLowerCase();

            if (!isEmailValid(contenido)) {
                String msg = "El E-mail no es valido";
                int duration = Toast.LENGTH_SHORT;
                Toast toast = Toast.makeText(context, msg, duration);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            } else {
                TaskAsynTask();
            }
        }
        if (v.equals(createLayout)) {
            startActivity(new Intent(context, AccountCreator.class));
            Login.this.overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        }
    }

    protected void TaskAsynTask() {
        String mailUp = mail.substring(0, 1).toUpperCase();
        mail = mailUp + mail.substring(1);
        new AsyncTask<Void, Void, Void>() {

            User user;

            @Override
            protected void onPreExecute() {
                ld.show();
                super.onPreExecute();
            }

            @Override
            protected Void doInBackground(Void... params) {
                user = new model_mysql.UserFacade().login(mail, pass);
                if (user != null) {
                    new model_sqlite.UserFacade(context).save(user);
                    List<Votes> votesList = new model_mysql.VotesFacade().voteses(mail);
                    for (Votes aVotesList : votesList) {
                        new VotesFacade(context).save(aVotesList);
                    }
                    List<Photography> photographyList = new PhotographysFacade().photographies(mail);
                    for (Photography aPhotographyList : photographyList) {
                        new model_sqlite.PhotographysFacade(context).save(aPhotographyList);
                    }
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                if (user != null) {
                    if (user.email.equals(mail) && user.password.equals(pass)) {
                        startActivity(new Intent(Login.this, Contest.class));
                        Login.this.overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                        finish();
                    } else {
                        String msg = "E-Mail no registrado";
                        int duration = Toast.LENGTH_SHORT;
                        Toast toast = Toast.makeText(context, msg, duration);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    }
                }
                ld.cancel();
                super.onPostExecute(aVoid);
            }

        }.execute();
    }

    protected void AsyntaskEvent() {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                Event eventmysql = new model_mysql.EventFacade().findEvent();
                Event eventsqlite = new model_sqlite.EventFacade(context).findEvent();
                if (eventsqlite == null) {
                    new model_sqlite.EventFacade(context).save(eventmysql);
                } else {
                    if (eventsqlite.EventId < eventmysql.EventId) {
                        new model_sqlite.EventFacade(context).save(eventmysql);
                    }
                }

                return null;
            }
        }.execute();
    }

}