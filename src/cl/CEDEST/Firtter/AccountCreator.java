package cl.CEDEST.Firtter;

import Elements.LoadingDialogs;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputType;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;
import components.*;
import controller.User;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AccountCreator extends Activity implements View.OnClickListener {

    String nick;
    String mail;
    String sexType;
    String helloText = "";

    int widthDisplay;
    int white = Color.WHITE;
    int verticalOrientation = LinearLayout.VERTICAL;
    int horizontalOrientation = LinearLayout.HORIZONTAL;
    int centralGravity = Gravity.CENTER;
    int rightGravity = Gravity.RIGHT;

    Context context;

    RelativeLayout main;

    RelativeLayout.LayoutParams cancelParams;
    RelativeLayout.LayoutParams logoParams;
    RelativeLayout.LayoutParams inputsParams0;
    RelativeLayout.LayoutParams inputsParams1;
    RelativeLayout.LayoutParams inputsParams2;
    RelativeLayout.LayoutParams inputsParams3;
    RelativeLayout.LayoutParams inputsParams4;
    RelativeLayout.LayoutParams inputsParams5;
    RelativeLayout.LayoutParams inputsParams6;
    RelativeLayout.LayoutParams buttonParams;
    RelativeLayout.LayoutParams avatarParams0;
    RelativeLayout.LayoutParams avatarParams1;
    RelativeLayout.LayoutParams finalParams;

    LinearLayout.LayoutParams cancelParam;
    LinearLayout.LayoutParams logoParam;
    LinearLayout.LayoutParams avatarParam;
    LinearLayout.LayoutParams inputsParam;
    LinearLayout.LayoutParams buttonParam;

    LinearLayout cancelLayout;
    LinearLayout logoLayout;
    LinearLayout inputsLayout0;
    LinearLayout inputsLayout1;
    LinearLayout inputsLayout2;
    LinearLayout inputsLayout3;
    LinearLayout inputsLayout4;
    LinearLayout inputsLayout5;
    LinearLayout inputsLayout6;
    LinearLayout buttonLayout;
    LinearLayout avatarLayout0;
    LinearLayout avatarLayout1;
    LinearLayout finalLayout;

    TextView cancelTextView;
    TextView spaceText0View;
    TextView spaceText1View;
    TextView spaceText2View;
    TextView helloTextView;

    ImageView logoIcon;
    ImageView avatar0;
    ImageView avatar1;
    ImageView avatar2;
    ImageView avatar3;
    ImageView avatar4;
    ImageView avatar5;

    EditText nickname;
    EditText email;
    EditText password;
    EditText repeatPassword;

    Button submit;

    LoadingDialogs ld;

    public boolean isEmailValid(String contenido) {
        String regExpn = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";

        CharSequence inputStr = contenido;

        Pattern pattern = Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);

        if (matcher.matches())
            return true;
        else
            return false;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);

        widthDisplay = getWindowManager().getDefaultDisplay().getWidth();

        context = this;

        ld = new LoadingDialogs(this);

        cancelParams = new RelativeLayout.LayoutParams(-1, -2);
        cancelParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        logoParams = new RelativeLayout.LayoutParams(-1, -2);
        logoParams.addRule(RelativeLayout.BELOW, 11);
        inputsParams0 = new RelativeLayout.LayoutParams(-1, -2);
        inputsParams0.addRule(RelativeLayout.BELOW, 12);
        inputsParams1 = new RelativeLayout.LayoutParams(-1, -2);
        inputsParams1.addRule(RelativeLayout.BELOW, 13);
        inputsParams2 = new RelativeLayout.LayoutParams(-1, -2);
        inputsParams2.addRule(RelativeLayout.BELOW, 14);
        inputsParams3 = new RelativeLayout.LayoutParams(-1, -2);
        inputsParams3.addRule(RelativeLayout.BELOW, 15);
        inputsParams4 = new RelativeLayout.LayoutParams(-1, -2);
        inputsParams4.addRule(RelativeLayout.BELOW, 16);
        inputsParams5 = new RelativeLayout.LayoutParams(-1, -2);
        inputsParams5.addRule(RelativeLayout.BELOW, 17);
        inputsParams6 = new RelativeLayout.LayoutParams(-1, -2);
        inputsParams6.addRule(RelativeLayout.BELOW, 18);
        buttonParams = new RelativeLayout.LayoutParams(-1, -1);
        buttonParams.addRule(RelativeLayout.BELOW, 19);
        avatarParams0 = new RelativeLayout.LayoutParams(-1, -2);
        avatarParams0.addRule(RelativeLayout.BELOW, 12);
        avatarParams1 = new RelativeLayout.LayoutParams(-1, -2);
        avatarParams1.addRule(RelativeLayout.BELOW, 20);
        finalParams = new RelativeLayout.LayoutParams(-1, -1);
        finalParams.addRule(RelativeLayout.BELOW, 21);

        avatarParam = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.FILL_PARENT);
        avatarParam.setMargins(widthDisplay / 20, widthDisplay / 40, widthDisplay / 20, widthDisplay / 40);
        cancelParam = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.FILL_PARENT);
        cancelParam.setMargins(0, widthDisplay * 3 / 100, 0, widthDisplay * 5 / 100);
        logoParam = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.FILL_PARENT);
        logoParam.setMargins(0, widthDisplay * 9 / 100, 0, widthDisplay * 8 / 100);
        inputsParam = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.FILL_PARENT);
        inputsParam.setMargins(0, widthDisplay * 1 / 100, 0, 0);
        buttonParam = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.FILL_PARENT);
        buttonParam.setMargins(0, widthDisplay / 10, 0, 0);

        main = new RelativeLayout(this, R.drawable.fondo);
        cancelLayout = new LinearLayout(this, "#80000000", rightGravity, horizontalOrientation);
        cancelLayout.setLayoutParams(cancelParams);
        cancelLayout.setId(11);
        cancelLayout.setOnClickListener(this);
        logoLayout = new LinearLayout(this, "#80000000", centralGravity, horizontalOrientation);
        logoLayout.setLayoutParams(logoParams);
        logoLayout.setId(12);
        inputsLayout0 = new LinearLayout(this, "#60000000", centralGravity, verticalOrientation);
        inputsLayout0.setLayoutParams(inputsParams0);
        inputsLayout0.setId(13);
        inputsLayout1 = new LinearLayout(this, "#80000000", centralGravity, verticalOrientation);
        inputsLayout1.setLayoutParams(inputsParams1);
        inputsLayout1.setId(14);
        inputsLayout2 = new LinearLayout(this, "#60000000", centralGravity, verticalOrientation);
        inputsLayout2.setLayoutParams(inputsParams2);
        inputsLayout2.setId(15);
        inputsLayout3 = new LinearLayout(this, "#80000000", centralGravity, verticalOrientation);
        inputsLayout3.setLayoutParams(inputsParams3);
        inputsLayout3.setId(16);
        inputsLayout4 = new LinearLayout(this, "#60000000", centralGravity, verticalOrientation);
        inputsLayout4.setLayoutParams(inputsParams4);
        inputsLayout4.setId(17);
        inputsLayout5 = new LinearLayout(this, "#80000000", centralGravity, verticalOrientation);
        inputsLayout5.setLayoutParams(inputsParams5);
        inputsLayout5.setId(18);
        inputsLayout6 = new LinearLayout(this, "#60000000", centralGravity, verticalOrientation);
        inputsLayout6.setLayoutParams(inputsParams6);
        inputsLayout6.setId(19);
        buttonLayout = new LinearLayout(this, "#80000000", centralGravity, verticalOrientation);
        buttonLayout.setLayoutParams(buttonParams);
        avatarLayout0 = new LinearLayout(this, "#00000000", centralGravity, horizontalOrientation);
        avatarLayout0.setLayoutParams(avatarParams0);
        avatarLayout0.setId(20);
        avatarLayout1 = new LinearLayout(this, "#00000000", centralGravity, horizontalOrientation);
        avatarLayout1.setLayoutParams(avatarParams1);
        avatarLayout1.setId(21);
        finalLayout = new LinearLayout(this, "#80000000", centralGravity, verticalOrientation);
        finalLayout.setLayoutParams(finalParams);

        String mystring = new String("   Back              ");
        SpannableString contenido = new SpannableString(mystring);
        contenido.setSpan(new UnderlineSpan(), 0, mystring.length(), 0);
        cancelTextView = new TextView(this, "", white, rightGravity);
        cancelTextView.setText(contenido);
        cancelTextView.setLayoutParams(cancelParam);
        spaceText0View = new TextView(this, " ", white, centralGravity);
        spaceText1View = new TextView(this, " ", white, centralGravity);
        spaceText2View = new TextView(this, " ", white, centralGravity);

        logoIcon = new ImageView(this, R.drawable.logo);
        logoIcon.setLayoutParams(logoParam);
        avatar0 = new ImageView(this, R.drawable.avatars_0000);
        avatar0.setLayoutParams(avatarParam);
        avatar0.setOnClickListener(this);
        avatar1 = new ImageView(this, R.drawable.avatars_0001);
        avatar1.setLayoutParams(avatarParam);
        avatar1.setOnClickListener(this);
        avatar2 = new ImageView(this, R.drawable.avatars_0002);
        avatar2.setLayoutParams(avatarParam);
        avatar2.setOnClickListener(this);
        avatar3 = new ImageView(this, R.drawable.avatars_0003);
        avatar3.setLayoutParams(avatarParam);
        avatar3.setOnClickListener(this);
        avatar4 = new ImageView(this, R.drawable.avatars_0004);
        avatar4.setLayoutParams(avatarParam);
        avatar4.setOnClickListener(this);
        avatar5 = new ImageView(this, R.drawable.avatars_0005);
        avatar5.setLayoutParams(avatarParam);
        avatar5.setOnClickListener(this);

        nickname = new EditText(this, "NICK", white, centralGravity);
        nickname.setInputType(InputType.TYPE_CLASS_TEXT);
        nickname.setLayoutParams(inputsParam);
        nickname.setBackgroundDrawable(getResources().getDrawable(R.drawable.inputshape));
        email = new EditText(this, "E-MAIL", white, centralGravity);
        email.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        email.setLayoutParams(inputsParam);
        email.setBackgroundDrawable(getResources().getDrawable(R.drawable.inputshape));
        password = new EditText(this, "PASSWORD", white, centralGravity);
        password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        password.setLayoutParams(inputsParam);
        password.setBackgroundDrawable(getResources().getDrawable(R.drawable.inputshape));
        repeatPassword = new EditText(this, "REPEAT PASSWORD", white, centralGravity);
        repeatPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        repeatPassword.setLayoutParams(inputsParam);
        repeatPassword.setBackgroundDrawable(getResources().getDrawable(R.drawable.inputshape));

        submit = new Button(this, "Submit", "#00000000", white);
        submit.setOnClickListener(this);
        submit.setBackgroundDrawable(getResources().getDrawable(R.drawable.cornershape));
        submit.setOnClickListener(this);

        cancelLayout.setVisibility(View.GONE);
        avatarLayout0.setVisibility(View.GONE);
        avatarLayout1.setVisibility(View.GONE);
        finalLayout.setVisibility(View.GONE);

        cancelLayout.addView(cancelTextView);
        logoLayout.addView(logoIcon);
        inputsLayout0.addView(nickname);
        inputsLayout1.addView(spaceText0View);
        inputsLayout2.addView(email);
        inputsLayout3.addView(spaceText1View);
        inputsLayout4.addView(password);
        inputsLayout5.addView(spaceText2View);
        inputsLayout6.addView(repeatPassword);
        buttonLayout.addView(submit);
        avatarLayout0.addView(avatar0);
        avatarLayout0.addView(avatar1);
        avatarLayout0.addView(avatar2);
        avatarLayout1.addView(avatar3);
        avatarLayout1.addView(avatar4);
        avatarLayout1.addView(avatar5);
        main.addView(cancelLayout);
        main.addView(logoLayout);
        main.addView(inputsLayout0);
        main.addView(inputsLayout1);
        main.addView(inputsLayout2);
        main.addView(inputsLayout3);
        main.addView(inputsLayout4);
        main.addView(inputsLayout5);
        main.addView(inputsLayout6);
        main.addView(avatarLayout0);
        main.addView(avatarLayout1);
        main.addView(finalLayout);
        main.addView(buttonLayout);

        submit.getLayoutParams().width = widthDisplay / 2;
        logoIcon.getLayoutParams().width = widthDisplay * 70 / 100;
        avatar0.getLayoutParams().height = widthDisplay * 20 / 100;
        avatar0.getLayoutParams().width = widthDisplay * 20 / 100;
        avatar1.getLayoutParams().height = widthDisplay * 20 / 100;
        avatar1.getLayoutParams().width = widthDisplay * 20 / 100;
        avatar2.getLayoutParams().height = widthDisplay * 20 / 100;
        avatar2.getLayoutParams().width = widthDisplay * 20 / 100;
        avatar3.getLayoutParams().height = widthDisplay * 20 / 100;
        avatar3.getLayoutParams().width = widthDisplay * 20 / 100;
        avatar4.getLayoutParams().height = widthDisplay * 20 / 100;
        avatar4.getLayoutParams().width = widthDisplay * 20 / 100;
        avatar5.getLayoutParams().height = widthDisplay * 20 / 100;
        avatar5.getLayoutParams().width = widthDisplay * 20 / 100;

        setContentView(main);
    }

    @Override
    public void onClick(View v) {
        if (v.equals(submit)) {
            nick = nickname.getText().toString();
            mail = email.getText().toString();
            String passw = password.getText().toString();
            String rptPwd = repeatPassword.getText().toString();
            mail = mail.toLowerCase();

            if (nickname == null) {
                String message = "El Usuario no es valido";
                int duration = Toast.LENGTH_LONG;
                Toast toast = Toast.makeText(context, message, duration);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                return;
            }
            if (!isEmailValid(mail)) {
                String message = "El E-mail no es valido";
                int duration = Toast.LENGTH_LONG;
                Toast toast = Toast.makeText(context, message, duration);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                return;
            }
            if (!passw.equals(rptPwd)) {
                String message = "Las Contraseñas no concuerdan";
                int duration = Toast.LENGTH_LONG;
                Toast toast = Toast.makeText(context, message, duration);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                return;
            } else {
                inputsLayout0.setVisibility(View.GONE);
                inputsLayout1.setVisibility(View.GONE);
                inputsLayout2.setVisibility(View.GONE);
                inputsLayout3.setVisibility(View.GONE);
                inputsLayout4.setVisibility(View.GONE);
                inputsLayout5.setVisibility(View.GONE);
                inputsLayout6.setVisibility(View.GONE);
                buttonLayout.setVisibility(View.GONE);
                cancelLayout.setVisibility(View.VISIBLE);
                avatarLayout0.setVisibility(View.VISIBLE);
                avatarLayout1.setVisibility(View.VISIBLE);
                finalLayout.setVisibility(View.VISIBLE);
                helloText = nick;
                helloTextView = new TextView(this, "Hello " + helloText + "\n  Please choose your Avatar", white, centralGravity);
                finalLayout.addView(helloTextView);
            }
        }
        if (v.equals(avatar0)) {
            String sex = "0";
            TaskAsynTask(sex);
        }
        if (v.equals(avatar1)) {
            String sex = "1";
            TaskAsynTask(sex);
        }
        if (v.equals(avatar2)) {
            String sex = "2";
            TaskAsynTask(sex);
        }
        if (v.equals(avatar3)) {
            String sex = "3";
            TaskAsynTask(sex);
        }
        if (v.equals(avatar4)) {
            String sex = "4";
            TaskAsynTask(sex);
        }
        if (v.equals(avatar5)) {
            String sex = "5";
            TaskAsynTask(sex);
        }
        if (v.equals(cancelLayout)) {
            inputsLayout0.setVisibility(View.VISIBLE);
            inputsLayout1.setVisibility(View.VISIBLE);
            inputsLayout2.setVisibility(View.VISIBLE);
            inputsLayout3.setVisibility(View.VISIBLE);
            inputsLayout4.setVisibility(View.VISIBLE);
            inputsLayout5.setVisibility(View.VISIBLE);
            inputsLayout6.setVisibility(View.VISIBLE);
            buttonLayout.setVisibility(View.VISIBLE);
            cancelLayout.setVisibility(View.GONE);
            avatarLayout0.setVisibility(View.GONE);
            avatarLayout1.setVisibility(View.GONE);
            finalLayout.setVisibility(View.GONE);
        }
    }

    protected void TaskAsynTask(String sex) {
        String mailUp = mail.substring(0, 1).toUpperCase();
        mail = mailUp + mail.substring(1);
        sexType = sex;
        new AsyncTask<Void, Void, Void>() {
            User user0;
            User user1;

            String em = mail;
            String passw = password.getText().toString();

            @Override
            protected void onPreExecute() {
                ld.show();
                super.onPreExecute();
            }

            @Override
            protected Void doInBackground(Void... params) {
                user0 = new User(nick, em, passw, sexType, -1);
                user1 = new model_mysql.UserFacade().find(em);
                if (user1 == null) {
                    user0 = new model_mysql.UserFacade().save(user0);
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                ld.cancel();
                if (user1 != null) {
                    String msg = "E-Mail no valido";
                    int duration = Toast.LENGTH_SHORT;
                    Toast toast = Toast
                            .makeText(context, msg, duration);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
                if (user1 == null) {
                    startActivity(new Intent(AccountCreator.this,
                            Login.class));
                    AccountCreator.this.overridePendingTransition(
                            android.R.anim.slide_in_left,
                            android.R.anim.slide_out_right);
                    finish();
                }
                super.onPostExecute(result);
            }
        }.execute();
    }
}