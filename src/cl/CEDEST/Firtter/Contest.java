package cl.CEDEST.Firtter;

import Elements.ExitDialogs;
import Elements.LoadingDialogs;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;
import components.*;
import components.ImageView;
import components.LinearLayout;
import components.RelativeLayout;
import components.TextView;
import controller.Event;
import controller.Photography;
import controller.ServerTime;
import controller.User;
import model_sqlite.EventFacade;
import model_sqlite.PhotographysFacade;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import Elements.TakeAvatar;
import model_sqlite.UserFacade;


public class Contest extends Activity implements View.OnClickListener {

    private static final int CAMERA_REQUEST = 1888;
    String name;
    String sex;
    String requestEvent0;
    String requestEvent1;
    int typeEvent;
    int day;
    int month;
    int year;
    int textSize0;
    int textSize1;
    int widthDisplay;
    int heightDisplay;
    int white = Color.WHITE;
    int verticalOrientation = LinearLayout.VERTICAL;
    int horizontalOrientation = LinearLayout.HORIZONTAL;
    int centralGravity = Gravity.CENTER;
    int rightGravity = Gravity.RIGHT;
    Context context;
    Calendar calendar;
    RelativeLayout.LayoutParams topLayoutParams;
    RelativeLayout.LayoutParams dateLayoutParams;
    RelativeLayout.LayoutParams eventGTVParams;
    RelativeLayout.LayoutParams requestTextParams0;
    RelativeLayout.LayoutParams requestTextParams1;
    RelativeLayout.LayoutParams photoTextParams;
    RelativeLayout.LayoutParams cameraImageParams;
    RelativeLayout.LayoutParams timerImageParams;
    RelativeLayout.LayoutParams timerTextParams;
    RelativeLayout.LayoutParams galleryLayoutParams;
    LinearLayout.LayoutParams dateLayoutParam;
    LinearLayout.LayoutParams avatarViewParam;
    LinearLayout.LayoutParams exitParam;
    LinearLayout.LayoutParams requestTextParam;
    LinearLayout.LayoutParams timerImageParam;
    LinearLayout.LayoutParams cameraImageParam;
    LinearLayout.LayoutParams timerTextParam;
    LinearLayout.LayoutParams timerTextParam2;
    LinearLayout.LayoutParams arrowUpParam;
    LinearLayout.LayoutParams galleryTextParam;
    RelativeLayout main;
    LinearLayout topLayout;
    LinearLayout contests0;
    LinearLayout contests1;
    LinearLayout dateLayout;
    LinearLayout requestLayout0;
    LinearLayout requestLayout1;
    LinearLayout photoTextLayout;
    LinearLayout cameraLayout;
    LinearLayout timerLayout;
    LinearLayout timerTextLayout;
    LinearLayout galleryLayout;
    TextView exitTextView;
    TextView timeTextView;
    TextView dateTextView;
    TextView requestTextView0;
    TextView requestTextView1;
    TextView photoTextView;
    TextView textUpperTimer;
    TextView textTimeContext;
    TextView galleryText;
    GreatTextView eventGTV0;
    GreatTextView eventGTV1;
    ImageView avatarImageView;
    ImageView cameraImageView;
    ImageView timerImageView;
    ImageView arrowUpImage;
    CounterTimeClass timer;
    LoadingDialogs ld;
    ExitDialogs ed;
    byte[] bArray;
    private Uri mImageUri;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
        widthDisplay = getWindowManager().getDefaultDisplay().getWidth();
        heightDisplay = getWindowManager().getDefaultDisplay().getHeight();
        context = this;
        AsyntaskEvent();

        if (widthDisplay < 1000) {
            textSize0 = heightDisplay / 25;
            textSize1 = heightDisplay / 50;
        } else {
            textSize0 = widthDisplay / 25;
            textSize1 = widthDisplay / 50;
        }

        ld = new LoadingDialogs(context);
        ed = new ExitDialogs(this);

        topLayoutParams = new RelativeLayout.LayoutParams(-1, -2);
        topLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        dateLayoutParams = new RelativeLayout.LayoutParams(-1, -2);
        dateLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        eventGTVParams = new RelativeLayout.LayoutParams(-1, -2);
        eventGTVParams.addRule(RelativeLayout.BELOW, 11);
        requestTextParams0 = new RelativeLayout.LayoutParams(-1, -2);
        requestTextParams0.addRule(RelativeLayout.BELOW, 12);
        requestTextParams1 = new RelativeLayout.LayoutParams(-1, -2);
        requestTextParams1.addRule(RelativeLayout.BELOW, 13);
        timerImageParams = new RelativeLayout.LayoutParams(-1, -2);
        timerImageParams.addRule(RelativeLayout.BELOW, 14);
        timerTextParams = new RelativeLayout.LayoutParams(-1, -2);
        timerTextParams.addRule(RelativeLayout.BELOW, 15);
        photoTextParams = new RelativeLayout.LayoutParams(-1, -1);
        photoTextParams.addRule(RelativeLayout.BELOW, 17);
        cameraImageParams = new RelativeLayout.LayoutParams(-1, -2);
        galleryLayoutParams = new RelativeLayout.LayoutParams(-1, -1);

        dateLayoutParam = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.FILL_PARENT);
        dateLayoutParam.setMargins(0, heightDisplay / 6, widthDisplay / 20, 0);
        avatarViewParam = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.FILL_PARENT);
        avatarViewParam.setMargins(widthDisplay / 3, widthDisplay * 2 / 100, 0, widthDisplay * 2 / 100);
        exitParam = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.FILL_PARENT);
        exitParam.setMargins(0, 0, widthDisplay / 3, 0);
        requestTextParam = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.FILL_PARENT);
        requestTextParam.setMargins(0, heightDisplay / 20, 0, heightDisplay / 20);
        timerImageParam = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.FILL_PARENT);
        timerImageParam.setMargins(0, heightDisplay / 10, 0, heightDisplay / 10);
        timerTextParam = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.FILL_PARENT);
        timerTextParam.setMargins(0, heightDisplay / 30, 0, 0);
        timerTextParam2 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.FILL_PARENT);
        timerTextParam2.setMargins(0, 0, 0, heightDisplay / 20);
        cameraImageParam = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.FILL_PARENT);
        cameraImageParam.setMargins(0, heightDisplay / 60, 0, heightDisplay / 40);
        arrowUpParam = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.FILL_PARENT);
        arrowUpParam.setMargins(0, 0, 0, heightDisplay * 3 / 100);
        galleryTextParam = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        galleryTextParam.setMargins(0, heightDisplay / 5, 0, 0);

        main = new RelativeLayout(this, R.drawable.fondo);
        topLayout = new LinearLayout(this, "#00000000", centralGravity, horizontalOrientation);
        topLayout.setBackgroundDrawable(getResources().getDrawable(R.drawable.botcornershape));
        topLayout.setLayoutParams(topLayoutParams);
        dateLayout = new LinearLayout(this, "#80000000", rightGravity, horizontalOrientation);
        dateLayout.setLayoutParams(dateLayoutParams);
        dateLayout.setId(11);
        contests0 = new LinearLayout(this, "#80000000", centralGravity, verticalOrientation);
        contests0.setLayoutParams(eventGTVParams);
        contests0.setId(12);
        contests1 = new LinearLayout(this, "#80000000", centralGravity, verticalOrientation);
        contests1.setLayoutParams(eventGTVParams);
        contests1.setId(13);
        requestLayout0 = new LinearLayout(this, "#80000000", centralGravity, verticalOrientation);
        requestLayout0.setLayoutParams(requestTextParams0);
        requestLayout0.setId(14);
        requestLayout1 = new LinearLayout(this, "#80000000", centralGravity, verticalOrientation);
        requestLayout1.setLayoutParams(requestTextParams1);
        requestLayout1.setId(15);
        timerLayout = new LinearLayout(this, "#80000000", centralGravity, verticalOrientation);
        timerLayout.setLayoutParams(timerImageParams);
        timerLayout.setId(16);
        cameraLayout = new LinearLayout(this, "#80000000", centralGravity, verticalOrientation);
        cameraLayout.setLayoutParams(cameraImageParams);
        cameraLayout.setId(17);
        timerTextLayout = new LinearLayout(this, "#80000000", centralGravity, verticalOrientation);
        timerTextLayout.setLayoutParams(timerTextParams);
        timerTextLayout.setId(18);
        photoTextLayout = new LinearLayout(this, "#80000000", centralGravity, verticalOrientation);
        photoTextLayout.setLayoutParams(photoTextParams);
        galleryLayout = new LinearLayout(this, "#80000000", centralGravity, verticalOrientation);
        galleryLayout.setLayoutParams(galleryLayoutParams);

        avatarImageView = new ImageView(this);
        cameraImageView = new ImageView(this, R.drawable.icons2_0012);
        cameraImageView.setLayoutParams(cameraImageParam);
        cameraImageView.getLayoutParams().height = widthDisplay / 5;
        cameraImageView.getLayoutParams().width = widthDisplay / 5;
        cameraImageView.setOnClickListener(this);
        timerImageView = new ImageView(this, R.drawable.timer);
        timerImageView.setLayoutParams(timerImageParam);
        timerImageView.getLayoutParams().height = widthDisplay / 4;
        timerImageView.getLayoutParams().width = widthDisplay / 4;
        arrowUpImage = new ImageView(this, R.drawable.icons_0008);
        arrowUpImage.setLayoutParams(arrowUpParam);
        arrowUpImage.getLayoutParams().height = widthDisplay / 15;
        arrowUpImage.getLayoutParams().width = widthDisplay / 15;
        arrowUpImage.setOnClickListener(this);

        calendar = Calendar.getInstance();
        day = calendar.get(Calendar.DAY_OF_MONTH);
        month = calendar.get(Calendar.MONTH);
        year = calendar.get(Calendar.YEAR);

        exitTextView = new TextView(this, "     Logout", white, centralGravity);
        exitTextView.setLayoutParams(exitParam);
        exitTextView.setOnClickListener(this);
        dateTextView = new TextView(this, day + "/" + (month + 1) + "/" + year, white, centralGravity);
        dateTextView.setLayoutParams(dateLayoutParam);
        eventGTV0 = new GreatTextView(this, "The First At", white, centralGravity);
        eventGTV0.setTextSize(textSize0);
        eventGTV1 = new GreatTextView(this, "The Best At", white, centralGravity);
        eventGTV1.setTextSize(textSize0);
        requestTextView0 = new TextView(this, requestEvent0, white, centralGravity);
        requestTextView0.setLayoutParams(requestTextParam);
        requestTextView1 = new TextView(this, requestEvent1, white, centralGravity);
        requestTextView1.setLayoutParams(requestTextParam);
        photoTextView = new TextView(this, "Submit Photo", white, centralGravity);
        textUpperTimer = new TextView(this, "T  I  M  E    L  E  F  T\nT  O    S  U  B  M  I  T", white, centralGravity);
        textUpperTimer.setTextSize(textSize1);
        timeTextView = new TextView(this, "00 | 00 | 00", white, centralGravity);
        timeTextView.setLayoutParams(timerTextParam);
        timeTextView.setTextSize(textSize0);
        textTimeContext = new TextView(this, "hrs    min    sec", white, centralGravity);
        textTimeContext.setLayoutParams(timerTextParam2);
        galleryText = new TextView(this, "Gallery", white, centralGravity);
        galleryText.setLayoutParams(galleryTextParam);

        topLayout.addView(exitTextView);
        dateLayout.addView(dateTextView);
        cameraLayout.addView(cameraImageView);
        photoTextLayout.addView(photoTextView);
        contests0.addView(eventGTV0);
        contests1.addView(eventGTV1);
        requestLayout0.addView(requestTextView0);
        requestLayout1.addView(requestTextView1);
        timerLayout.addView(timerImageView);
        timerTextLayout.addView(textUpperTimer);
        timerTextLayout.addView(timeTextView);
        timerTextLayout.addView(textTimeContext);
        galleryLayout.addView(galleryText);
        galleryLayout.addView(arrowUpImage);

        main.addView(dateLayout);
        main.addView(photoTextLayout);
        main.addView(cameraLayout);
        main.addView(topLayout);

        User user = new model_sqlite.UserFacade(context).find();
        Event event = new model_sqlite.EventFacade(context).findEvent();
        if (user != null) {
            name = user.nickname;
            sex = user.sex;
            typeEvent = event.EventType;
            new TakeAvatar(context, sex, avatarImageView, topLayout, avatarViewParam, widthDisplay);
            takeEvent();
            AsyntaskT0();
        } else {
            startActivity(new Intent(context, Login.class));
            finish();
        }

        setContentView(main);
    }

    /**
     * Click Listeners **
     */
    @Override
    public void onClick(View v) {
        if (v.equals(avatarImageView)) {
            startActivity(new Intent(Contest.this, Profile.class));
            Contest.this.overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        }
        if (v.equals(exitTextView)) {
            ed.show();
        }
        if (v.equals(cameraImageView)) {
            Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            File photo = null;
            try {
                photo = this.createTemporaryFile("picture", ".jpg");
                photo.delete();
            } catch (Exception e) {
                Toast.makeText(context, "Please check SD card! Image shot is impossible!", Toast.LENGTH_SHORT);
                System.out.println("Error en crear la imagen --->" + e);
            }
            mImageUri = Uri.fromFile(photo);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);
            //start camera intent
            startActivityForResult(intent, CAMERA_REQUEST);
        }
        if (v.equals(arrowUpImage)) {
            startActivity(new Intent(Contest.this, Gallery.class));
            Contest.this.overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        }
    }


    /**
     * Camera get photo **
     */
    public void grabImage() {
        this.getContentResolver().notifyChange(mImageUri, null);
        ContentResolver cr = this.getContentResolver();
        Bitmap bitmap;
        Bitmap brezised;
        try {
            bitmap = android.provider.MediaStore.Images.Media.getBitmap(cr, mImageUri);
            int widh = bitmap.getWidth(), height = bitmap.getHeight();
            if (height > widh) {
                brezised = bitmap.createScaledBitmap(bitmap, 563, 1000, false);
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                brezised.compress(Bitmap.CompressFormat.JPEG, 80, bos);
                byte[] bitmapdata = bos.toByteArray();
                bArray = bitmapdata;
            }
            if (widh > height) {
                brezised = bitmap.createScaledBitmap(bitmap, 1000, 563, false);
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                brezised.compress(Bitmap.CompressFormat.JPEG, 80, bos);
                byte[] bitmapdata = bos.toByteArray();
                bArray = bitmapdata;
            }

            AsynTaskPhoto(bArray);
        } catch (Exception e) {
            Toast.makeText(this, "Failed to load", Toast.LENGTH_SHORT).show();
            System.out.println("Failed to load -->" + e);
        }
    }

    private File createTemporaryFile(String part, String ext) throws Exception {
        File tempDir = Environment.getExternalStorageDirectory();
        tempDir = new File(tempDir.getAbsolutePath() + "/.temp/");
        if (!tempDir.exists()) {
            tempDir.mkdir();
        }
        return File.createTempFile(part, ext, tempDir);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {
            this.grabImage();
        }
        super.onActivityResult(requestCode, resultCode, intent);
    }


    /**
     * takeEvent **
     */
    public void takeEvent() {
        if (typeEvent == 1) {
            eventType1();
        }
        if (typeEvent == 2) {
            eventType2();
        }
    }

    public void eventType1() {
        int EventId = new EventFacade(context).findEvent().EventId;
        int photoEventID = 0;
        requestTextView0.setText(new EventFacade(context).findEvent().EventRequest);
        cameraImageParams.addRule(RelativeLayout.BELOW, 16);
        try {
            photoEventID = new PhotographysFacade(context).findPhotEvent(EventId).event;
        } catch (Exception e) {
            System.out.println("cant try ---->" + e);
        }

        if (photoEventID != EventId) {
            main.addView(contests0);
            main.addView(requestLayout0);
            main.addView(timerLayout);
        } else {
            galleryLayoutParams.addRule(RelativeLayout.BELOW, 16);
            try {
                main.removeView(photoTextLayout);
                main.removeView(cameraLayout);
                main.removeView(contests0);
                main.removeView(requestLayout0);
                main.removeView(timerLayout);
            } catch (Error error) {
                System.out.println("Error ---> " + error);
            }
            main.addView(galleryLayout);
            main.addView(contests0);
            main.addView(requestLayout0);
            main.addView(timerLayout);
        }
    }

    public void eventType2() {

        int photoEventID = 0;
        int EventId = new EventFacade(context).findEvent().EventId;
        requestTextView1.setText(new EventFacade(context).findEvent().EventRequest);
        cameraImageParams.addRule(RelativeLayout.BELOW, 18);
        try {
            photoEventID = new PhotographysFacade(context).findPhotEvent(EventId).event;
            System.out.println(photoEventID);
        } catch (Exception e) {
            System.out.println("cant try ---->" + e);
        }

        if (photoEventID != EventId) {
            main.addView(contests1);
            main.addView(requestLayout1);
            main.addView(timerTextLayout);
        } else {
            galleryLayoutParams.addRule(RelativeLayout.BELOW, 18);
            try {
                main.removeView(photoTextLayout);
                main.removeView(cameraLayout);
                main.removeView(contests1);
                main.removeView(requestLayout1);
                main.removeView(timerTextLayout);
            } catch (Error error) {
                System.out.println("Error ---> " + error);
            }
            main.addView(galleryLayout);
            main.addView(contests1);
            main.addView(requestLayout1);
            main.addView(timerTextLayout);
        }
    }

    /**
     * calculador de tiempo **
     */
    public void timeCalc(ServerTime serverTime) {

        String today = serverTime.toString();
        String eventFinishTime = new EventFacade(context).findEvent().FTermino;
        SimpleDateFormat format = new SimpleDateFormat("yy-MM-dd HH:mm:ss");
        Date d1 = null;
        Date d2 = null;
        try {
            d1 = format.parse(today);
            d2 = format.parse(eventFinishTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        long diff = d2.getTime() - d1.getTime();

        timer = new CounterTimeClass(diff, 1000);
        timer.start();
    }

    /**
     * Asyntask **
     */
    protected void AsyntaskT0() {

        new AsyncTask<Void, Void, Void>() {
            ServerTime serverTime;

            @Override
            protected void onPreExecute() {
                ld.show();
                super.onPreExecute();
            }

            @Override
            protected Void doInBackground(Void... params) {
                serverTime = new model_mysql.ServerTimeFacade().find();
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                timeCalc(serverTime);
                ld.cancel();
                super.onPostExecute(aVoid);
            }
        }.execute();
    }

    protected void AsyntaskEvent() {
        new AsyncTask<Void, Void, Void>() {
            Event eventmysql;
            Event eventsqlite;

            @Override
            protected Void doInBackground(Void... params) {
                eventmysql = new model_mysql.EventFacade().findEvent();

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                eventsqlite = new model_sqlite.EventFacade(context).findEvent();
                if (eventsqlite == null) {
                    new model_sqlite.EventFacade(context).save(eventmysql);
                } else {
                    if (eventsqlite.EventId < eventmysql.EventId) {
                        new model_sqlite.EventFacade(context).save(eventmysql);
                    }
                }
                super.onPostExecute(aVoid);
            }
        }.execute();
    }

    protected void AsynTaskPhoto(final byte[] biteArray) {

        new AsyncTask<Void, Void, Void>() {
            Photography photo;
            byte[] bArray = biteArray;

            @Override
            protected void onPreExecute() {
                ld.show();
                super.onPreExecute();
            }

            @Override
            protected Void doInBackground(Void... params) {

                String localEmail = new UserFacade(context).find().email;
                int localEvent = new EventFacade(context).findEvent().EventId;
                photo = new model_mysql.PhotographysFacade().findPhoto(
                        localEmail, localEvent);
                if (photo == null) {
                    photo = new Photography(bArray, localEmail, new EventFacade(context).findEvent().EventId, 1, 0);
                    new model_mysql.PhotographysFacade().save(photo);
                    new PhotographysFacade(context).save(photo);
                }
                return null;
            }
            @Override
            protected void onPostExecute(Void result) {
                takeEvent();
                ld.cancel();
                super.onPostExecute(result);
            }
        }.execute();

    }

    /**
     * Timer **
     */
    public class CounterTimeClass extends CountDownTimer {

        public CounterTimeClass(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            long millis = millisUntilFinished;
            String hms = String.format("%02d | %02d | %02d", TimeUnit.MILLISECONDS.toHours(millis),
                    TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                    TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
            timeTextView.setText(hms);
        }

        @Override
        public void onFinish() {
            timeTextView.setText("done");
        }
    }
}