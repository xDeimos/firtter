package cl.CEDEST.Firtter.Fragments;

import Elements.LoadingDialogs;
import Elements.Adapter;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import cl.CEDEST.Firtter.Gallery;
import controller.ImageAdapter;
import components.LinearLayout;
import controller.Photography;

import java.util.List;


public class FragmentGallery2 extends Fragment {

    public Gallery gallery;
    int height;
    int width;
    DisplayMetrics displaymetrics;
    LinearLayout main;
    GridView gridView;
    Adapter adapter;
    Context context;
    LoadingDialogs ld;

    public FragmentGallery2(Gallery gallery) {
        this.gallery = gallery;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        context = inflater.getContext();
        ld = new LoadingDialogs(context);

        adapter = new Adapter(context);

        displaymetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        height = displaymetrics.heightPixels;
        width = displaymetrics.widthPixels;

        main = new LinearLayout(context);
        gridView = new GridView(context);
        gridView.setNumColumns(2);
        gridView.setColumnWidth(width / 2);
        gridView.setAdapter(adapter);

        main.addView(gridView);
        main.setGravity(Gravity.CENTER);
        getImages(this.gallery.photographies);
        return main;
    }


    public void getImages(List<Photography> photographies) {
        for (Photography photography : photographies) {
            byte[] bytes = photography.photo;
            int photoid = photography.photoid;
            int evenid = photography.event;
            String email = photography.email;
            Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
            adapter.add(new ImageAdapter(bitmap, photoid, evenid, email, 2, width, height));
        }

    }
}
