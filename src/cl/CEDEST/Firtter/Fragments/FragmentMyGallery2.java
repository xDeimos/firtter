package cl.CEDEST.Firtter.Fragments;

import Elements.LoadingDialogs;
import Elements.MyAdapter;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import cl.CEDEST.Firtter.MyGallery;
import components.LinearLayout;
import controller.MyImageAdapter;
import controller.Photography;

import java.util.List;


public class FragmentMyGallery2 extends Fragment {

    public MyGallery gallery;
    int height;
    int width;
    DisplayMetrics displaymetrics;
    LinearLayout main;
    GridView gridView;
    MyAdapter adapter;
    Context context;
    LoadingDialogs ld;

    public FragmentMyGallery2(MyGallery gallery) {
        this.gallery = gallery;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        context = inflater.getContext();
        ld = new LoadingDialogs(context);

        adapter = new MyAdapter(context);

        displaymetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        height = displaymetrics.heightPixels;
        width = displaymetrics.widthPixels;

        main = new LinearLayout(context);
        gridView = new GridView(context);
        gridView.setNumColumns(2);
        gridView.setColumnWidth(width / 2);
        gridView.setAdapter(adapter);

        main.addView(gridView);
        main.setGravity(Gravity.CENTER);
        getImages(this.gallery.photographies);
        return main;
    }


    public void getImages(List<Photography> photographies) {
        for (Photography photography : photographies) {
            byte[] bytes = photography.photo;
            int photoid = photography.photoid;
            Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
            adapter.add(new MyImageAdapter(bitmap, photoid, 2, width, height));
        }

    }
}
