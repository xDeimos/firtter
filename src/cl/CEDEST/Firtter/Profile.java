package cl.CEDEST.Firtter;

import Elements.ExitDialogs;
import Elements.LoadingDialogs;
import Elements.TakeAvatarProfile;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.view.*;
import components.*;
import components.ImageView;
import components.LinearLayout;
import components.RelativeLayout;
import components.TextView;
import controller.Photography;
import controller.User;
import model_sqlite.UserFacade;

public class Profile extends Activity implements View.OnClickListener {

    int textSize1;
    int widthDisplay;
    int heightDisplay;
    int white = Color.WHITE;
    int verticalOrientation = LinearLayout.VERTICAL;
    int horizontalOrientation = LinearLayout.HORIZONTAL;
    int centralGravity = Gravity.CENTER;

    RelativeLayout.LayoutParams topLayoutParams;
    RelativeLayout.LayoutParams contentLayoutParams0;
    RelativeLayout.LayoutParams contentLayoutparams1;
    RelativeLayout.LayoutParams avatarImageParams;

    LinearLayout.LayoutParams contesTextParam;
    LinearLayout.LayoutParams exitTextParam;
    LinearLayout.LayoutParams avatarViewParam;
    LinearLayout.LayoutParams nameTextParam;
    LinearLayout.LayoutParams lineImageParam;

    RelativeLayout main;
    LinearLayout topLayout;
    LinearLayout avatarImageLayout;
    LinearLayout contentLayout0;
    LinearLayout contentLayout1;

    TextView exitTextView;
    TextView contestTextView;
    TextView nameTextView;
    TextView favoriteTextView;
    TextView myphotosTextView;

    ImageView avatarImageView;
    ImageView line;

    Context context;

    ExitDialogs ed;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
        widthDisplay = getWindowManager().getDefaultDisplay().getWidth();
        heightDisplay = getWindowManager().getDefaultDisplay().getHeight();
        context = this;

        ed = new ExitDialogs(this);

        if (widthDisplay < 1000) {
            textSize1 = heightDisplay / 50;
        } else {
            textSize1 = widthDisplay / 50;
        }

        User user = new UserFacade(context).find();

        topLayoutParams = new RelativeLayout.LayoutParams(-1, -2);
        topLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        contentLayoutParams0 = new RelativeLayout.LayoutParams(-1, -2);
        contentLayoutParams0.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        avatarImageParams = new RelativeLayout.LayoutParams(-1, -2);
        avatarImageParams.addRule(RelativeLayout.BELOW, 11);
        contentLayoutparams1 = new RelativeLayout.LayoutParams(-1, -1);
        contentLayoutparams1.addRule(RelativeLayout.BELOW, 12);

        contesTextParam = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.FILL_PARENT);
        contesTextParam.setMargins(widthDisplay / 3, widthDisplay * 40 / 1000, 0, widthDisplay * 45 / 1000);
        exitTextParam = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.FILL_PARENT);
        exitTextParam.setMargins(0, 0, widthDisplay / 3, 0);
        avatarViewParam = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.FILL_PARENT);
        avatarViewParam.setMargins(0, heightDisplay * 3 / 100, 0, heightDisplay * 3 / 100);
        nameTextParam = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.FILL_PARENT);
        nameTextParam.setMargins(0, heightDisplay / 5, 0, 0);
        lineImageParam = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.FILL_PARENT);
        lineImageParam.setMargins(0, heightDisplay / 50, 0, heightDisplay / 50);

        main = new RelativeLayout(this, R.drawable.fondo);
        topLayout = new LinearLayout(this, "#00000000", centralGravity, horizontalOrientation);
        topLayout.setBackgroundDrawable(getResources().getDrawable(R.drawable.botcornershape));
        topLayout.setLayoutParams(topLayoutParams);
        contentLayout0 = new LinearLayout(this, "#80000000", centralGravity, verticalOrientation);
        contentLayout0.setLayoutParams(contentLayoutParams0);
        contentLayout0.setId(11);
        contentLayout1 = new LinearLayout(this, "#80000000", centralGravity, verticalOrientation);
        contentLayout1.setLayoutParams(contentLayoutparams1);
        avatarImageLayout = new LinearLayout(this, "#80000000", centralGravity, verticalOrientation);
        avatarImageLayout.setLayoutParams(avatarImageParams);
        avatarImageLayout.setId(12);

        avatarImageView = new ImageView(this);
        line = new ImageView(this, R.drawable.bar);
        line.setLayoutParams(lineImageParam);

        String newnickname = "";
        for (int i = 0; i < user.nickname.length(); i++) {
            newnickname += user.nickname.substring(i, i + 1).concat(" ").toUpperCase();
        }
        exitTextView = new TextView(this, "     Logout", white, centralGravity);
        exitTextView.setLayoutParams(exitTextParam);
        exitTextView.setOnClickListener(this);
        contestTextView = new TextView(this, "Contest     ", white, centralGravity);
        contestTextView.setLayoutParams(contesTextParam);
        contestTextView.setOnClickListener(this);
        nameTextView = new TextView(this, newnickname, white, centralGravity);
        nameTextView.setLayoutParams(nameTextParam);
        nameTextView.setTextSize(textSize1);
        favoriteTextView = new TextView(this, "F A V O U R I T E S", white, centralGravity);
        favoriteTextView.setOnClickListener(this);
        favoriteTextView.setTextSize(textSize1);
        myphotosTextView = new TextView(this, "M Y  P H O T O S", white, centralGravity);
        myphotosTextView.setOnClickListener(this);
        myphotosTextView.setTextSize(textSize1);

        topLayout.addView(exitTextView);
        topLayout.addView(contestTextView);
        contentLayout0.addView(nameTextView);
        contentLayout1.addView(favoriteTextView);
        contentLayout1.addView(line);
        contentLayout1.addView(myphotosTextView);
        main.addView(contentLayout0);
        main.addView(avatarImageLayout);
        main.addView(contentLayout1);
        main.addView(topLayout);

        new TakeAvatarProfile(user.sex, avatarImageView, avatarImageLayout,
                avatarViewParam, widthDisplay);

        line.getLayoutParams().width = widthDisplay * 60 / 100;
        setContentView(main);
        line.getLayoutParams().height = heightDisplay / 50;
    }

    @Override
    public void onClick(View v) {
        if (v.equals(exitTextView)) {
            ed.show();
        }
        if (v.equals(contestTextView)) {
            Profile.this.overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
            finish();
        }
        if (v.equals(myphotosTextView)) {
            startActivity(new Intent(Profile.this, MyGallery.class));
        }
        if (v.equals(favoriteTextView)) {
            System.out.println("hice click en favorito :)");
        }
    }
}