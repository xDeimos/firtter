package cl.CEDEST.Firtter;

import Elements.ExitDialogs;
import Elements.LoadingDialogs;
import Elements.TakeAvatar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import cl.CEDEST.Firtter.Fragments.FragmentGallery1;
import cl.CEDEST.Firtter.Fragments.FragmentGallery2;
import cl.CEDEST.Firtter.Fragments.FragmentGallery3;
import components.ImageView;
import components.LinearLayout;
import components.RelativeLayout;
import components.TextView;
import controller.Photography;
import controller.User;
import controller.Votes;
import model_mysql.PhotographysFacade;
import model_sqlite.EventFacade;
import model_sqlite.UserFacade;
import model_sqlite.VotesFacade;

import java.util.ArrayList;
import java.util.List;

public class Gallery extends FragmentActivity implements View.OnClickListener {

    public List<Photography> photographies;
    int widthDisplay;
    int heightDisplay;
    int white = Color.WHITE;
    int horizontalOrientation = LinearLayout.HORIZONTAL;
    int centralGravity = Gravity.CENTER;
    RelativeLayout.LayoutParams topLayoutParams;
    RelativeLayout.LayoutParams botLayoutParams;
    LinearLayout.LayoutParams avatarViewParam;
    LinearLayout.LayoutParams exitParam;
    LinearLayout.LayoutParams picsParam;
    RelativeLayout main;
    LinearLayout topLayout;
    LinearLayout botLayout;
    ViewPager view;
    ImageView avatarImageView;
    ImageView onePic;
    ImageView twoPic;
    ImageView threePic;
    TextView exitTextView;
    ExitDialogs ed;
    Context context;
    LoadingDialogs ld;
    List<Fragment> listado;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
        widthDisplay = getWindowManager().getDefaultDisplay().getWidth();
        heightDisplay = getWindowManager().getDefaultDisplay().getHeight();

        context = this;

        ed = new ExitDialogs((Activity) context);
        ld = new LoadingDialogs(context);

        AsynTaskPhotoList();

        topLayoutParams = new RelativeLayout.LayoutParams(-1, -2);
        topLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        botLayoutParams = new RelativeLayout.LayoutParams(-1, -2);
        botLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);

        avatarViewParam = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.FILL_PARENT);
        avatarViewParam.setMargins(widthDisplay / 3, widthDisplay * 2 / 100, 0, widthDisplay * 2 / 100);
        exitParam = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.FILL_PARENT);
        exitParam.setMargins(0, 0, widthDisplay / 3, 0);
        picsParam = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.FILL_PARENT);
        picsParam.setMargins(widthDisplay / 10, heightDisplay / 50, widthDisplay / 10, heightDisplay / 50);

        main = new RelativeLayout(this);
        topLayout = new LinearLayout(this, "#00000000", centralGravity, horizontalOrientation);
        topLayout.setBackgroundDrawable(getResources().getDrawable(R.drawable.botcornershape));
        topLayout.setLayoutParams(topLayoutParams);
        botLayout = new LinearLayout(this, "#80000000", centralGravity, horizontalOrientation);
        botLayout.setLayoutParams(botLayoutParams);

        exitTextView = new TextView(this, "     Logout", white, centralGravity);
        exitTextView.setLayoutParams(exitParam);
        exitTextView.setOnClickListener(this);

        avatarImageView = new ImageView(this);
        onePic = new ImageView(this, R.drawable.icons_0012);
        onePic.setLayoutParams(picsParam);
        onePic.setOnClickListener(this);
        twoPic = new ImageView(this, R.drawable.icons_0011);
        twoPic.setLayoutParams(picsParam);
        twoPic.setOnClickListener(this);
        threePic = new ImageView(this, R.drawable.icons_0010);
        threePic.setLayoutParams(picsParam);
        threePic.setOnClickListener(this);

        view = new ViewPager(this);
        view.setId(12);

        listado = new ArrayList<Fragment>();
        listado.add(new FragmentGallery1(this));
        listado.add(new FragmentGallery2(this));
        listado.add(new FragmentGallery3(this));
        CollectionFragment collectionFragment = new CollectionFragment(this.getSupportFragmentManager(), listado);
        view.setAdapter(collectionFragment);

        User user = new model_sqlite.UserFacade(context).find();
        String sex = user.sex;

        topLayout.addView(exitTextView);
        botLayout.addView(onePic);
        botLayout.addView(twoPic);
        botLayout.addView(threePic);

        new TakeAvatar(context, sex, avatarImageView, topLayout, avatarViewParam, widthDisplay);

        onePic.getLayoutParams().width = widthDisplay / 10;
        onePic.getLayoutParams().height = widthDisplay / 10;
        twoPic.getLayoutParams().width = widthDisplay / 10;
        twoPic.getLayoutParams().height = widthDisplay / 10;
        threePic.getLayoutParams().width = widthDisplay / 10;
        threePic.getLayoutParams().height = widthDisplay / 10;

        setContentView(main);
    }

    @Override
    public void onClick(View v) {
        if (v.equals(avatarImageView)) {
            startActivity(new Intent(Gallery.this, Profile.class));
            Gallery.this.overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
            finish();
        }
        if (v.equals(exitTextView)) {
            ed.show();
        }
        if (v.equals(onePic)) {

        }
        if (v.equals(twoPic)) {

        }
        if (v.equals(threePic)) {

        }
    }
    public void AsynTaskPhotoList() {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected void onPreExecute() {
                ld.show();
                super.onPreExecute();
            }

            @Override
            protected Void doInBackground(Void... params) {
                int idEvent = new EventFacade(context).findEvent().EventId;
                photographies = new PhotographysFacade().photographiesEvent(idEvent);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                main.addView(view);
                main.addView(topLayout);
                main.addView(botLayout);
                ld.cancel();
                super.onPostExecute(aVoid);
            }
        }.execute();
    }

}