package components;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;

/**
 * Created by deimos on 07-10-14.
 */
public class LinearLayout extends android.widget.LinearLayout {

    public LinearLayout(Context context) {
        super(context);
    }

    public LinearLayout(Context context, String color, int gravity, int orientation) {
        this(context);
        this.setBackgroundColor(Color.parseColor(color));
        this.setGravity(gravity);
        this.setOrientation(orientation);
    }

    public LinearLayout(Context context, int imageDrawable, int gravity, int orientation) {
        this(context);
        Drawable drawable = this.getResources().getDrawable(imageDrawable);
        this.setBackgroundDrawable(drawable);
        this.setGravity(gravity);
        this.setOrientation(orientation);
    }

}