package components;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;

/**
 * Created by deimos on 07-11-14.
 */
public class GreatTextView extends android.widget.TextView {
    public GreatTextView(Context context) {
        super(context);
        Typeface tp = Typeface.createFromAsset(context.getAssets(), "Rakoon_PersonalUse.ttf");
        this.setTypeface(tp);
    }

    public GreatTextView(Context context, String text, int color, int gravity) {
        this(context);
        this.setText(text);
        this.setTextColor(color);
        this.setGravity(gravity);
    }

    public GreatTextView(Context context, String text, String color, int gravity) {
        this(context);
        this.setText(text);
        this.setTextColor(Color.parseColor(color));
        this.setGravity(gravity);
    }

}
