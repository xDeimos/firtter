package components;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;

public class EditText extends android.widget.EditText {
    public EditText(Context context) {
        super(context);
        Typeface tp = Typeface.createFromAsset(context.getAssets(), "FuturaLTLight.ttf");
        this.setTypeface(tp);

        // TODO Auto-generated constructor stub
    }

    public EditText(Context context, String texto, int color, int gravedad) {
        this(context);
        this.setHint(texto);
        this.setHintTextColor(color);
        this.setGravity(gravedad);
        this.setTextColor(Color.WHITE);
    }
}
