package components;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;

/**
 * Created by deimos on 07-10-14.
 */
public class TextView extends android.widget.TextView {
    public TextView(Context context) {
        super(context);
        Typeface tp = Typeface.createFromAsset(context.getAssets(), "FuturaLTLight.ttf");
        this.setTypeface(tp);
    }

    public TextView(Context context, String text, int color, int gravity) {
        this(context);
        this.setText(text);
        this.setTextColor(color);
        this.setGravity(gravity);
    }

    public TextView(Context context, String text, String color, int gravity) {
        this(context);
        this.setText(text);
        this.setTextColor(Color.parseColor(color));
        this.setGravity(gravity);
    }
}
