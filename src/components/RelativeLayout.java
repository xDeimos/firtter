package components;

import android.content.Context;
import android.graphics.drawable.Drawable;

/**
 * Created by deimos on 08-10-14.
 */
public class RelativeLayout extends android.widget.RelativeLayout {
    public RelativeLayout(Context context) {
        super(context);
    }

    public RelativeLayout(Context context, int imageDrawable) {
        super(context);
        Drawable drawable = getResources().getDrawable(imageDrawable);
        this.setBackgroundDrawable(drawable);
    }

    public RelativeLayout(Context context, int gravity, int imageDrawable) {
        super(context);
        this.setGravity(gravity);
        Drawable drawable = getResources().getDrawable(imageDrawable);
        this.setBackgroundDrawable(drawable);
    }
}
