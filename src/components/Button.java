package components;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;

/**
 * Created by deimos on 08-10-14.
 */
public class Button extends android.widget.Button {
    public Button(Context context) {
        super(context);
        Typeface tp = Typeface.createFromAsset(context.getAssets(), "FuturaLTBook.ttf");
        this.setTypeface(tp);
    }

    public Button(Context context, String texto, String color, int colortext) {
        this(context);
        this.setText(texto);
        this.setBackgroundColor(Color.parseColor(color));
        this.setTextColor(colortext);
    }
}