package components;

import android.content.Context;
import android.graphics.drawable.Drawable;

/**
 * Created by deimos on 07-10-14.
 */
public class ImageView extends android.widget.ImageView {
    public ImageView(Context context) {
        super(context);
    }

    public ImageView(Context context, int imageDrawable) {
        this(context);
        Drawable drawable = getResources().getDrawable(imageDrawable);
        this.setImageDrawable(drawable);
    }
}
